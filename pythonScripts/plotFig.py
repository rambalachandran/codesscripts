'''
## https://stackoverflow.com/questions/3428532/how-to-import-a-csv-file-using-python-with-headers-intact-where-first-column-is
import csv
from pprint import pprint
inFile = '/project/projectdirs/m2113/rbala/Perovskites/BZO_NEB/energyVals_IMAGES3.csv'
with open( inFile, "rb" ) as theFile:
    reader = csv.DictReader( theFile )
    #print pprint(reader)
    for line in reader:
        print line
       
'''
import numpy as np
import matplotlib.pyplot as plt
import os,sys
inPath = '/project/projectdirs/m2113/rbala/Perovskites/BZO_NEB/'
inFileName = 'energyVals_IMAGES5_CFR.csv'
outFileName = 'NEB_IMAGES5_CFR_v3.eps'

'''
inFile = os.path.join(inPath,inFileName)
nameList = ['IMG', 'CFR', 'CSD', 'NCFR', 'NCSD']
data = np.genfromtxt(inFile, delimiter=',', skip_header=1,
                     skip_footer=0,names=nameList)
for name in nameList:
    data[name]=data[name]-data[name][0]

plt.plot(data['IMG'],data['CFR'], 'k-o', label='Cluster-FullRelax') #plot1
plt.plot(data['IMG'],data['CSD'], 'r-^', label='Cluster-SD') #plot2
plt.plot(data['IMG'],data['NCFR'], 'g-s', label='NO-Cluster-FullRelax') #plot3
plt.plot(data['IMG'],data['NCSD'], 'b-D', label='NO-Cluster-SD') #plot3

'''

inFile = os.path.join(inPath,inFileName)
nameList = ['IMG', 'KPT_1X1X1', 'KPT_1X1X1_v2', 'KPT_2X2X2', 'KPT_1X1X1_HSE']
data = np.genfromtxt(inFile, delimiter=',', skip_header=1,
                     skip_footer=0,names=nameList)
print data, data['KPT_1X1X1_HSE']

for name in nameList:
    data[name]=data[name]-data[name][0]
    
print data
plt.plot(data['IMG'],data['KPT_1X1X1'], 'k-s', label='Cluster-FR-Kpt_1x1x1') #plot1
plt.plot(data['IMG'],data['KPT_1X1X1_v2'], 'k-^', label='Cluster-FR-Kpt_1x1x1_v2') #plot2
plt.plot(data['IMG'],data['KPT_2X2X2'], 'k-o', label='Cluster-FR-Kpt_2x2x2') #plot3
plt.plot(data['IMG'],data['KPT_1X1X1_HSE'], 'r-^', label='Cluster-FR-Kpt_1x1x1_HSE') #plot4

plt.legend(loc='upper right',prop={'size':12})
plt.xlabel('IMAGE.NO')
plt.ylabel('Transition Energy (eV)')
plt.savefig(os.path.join(inPath,outFileName), format='eps', dpi=1000)
plt.close()

                     
'''
### STRANGE ERROR
for name in nameList:
    for cnt, val in enumerate(data[name]):
        print data[name][cnt], data[name][0], data[name][cnt]-data[name][0]
        data[name][cnt] = data[name][cnt]-data[name][0]
        #print name, cnt, val, data[name][cnt], data[name][0]
        #data[name][cnt] = val - data[name][0]
'''

