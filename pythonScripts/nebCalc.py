import os, sys
from pprint import pprint
import numpy as np
import pymatgen as mg
from pymatgen.core.structure import Structure
from pymatgen.io.vaspio import Poscar

import interstitial

class neb:
    def rotate_interstitial(self, crystalStrucInit, crystalStrucFinal,
			    intstIndex, neighborIndex, noImages, dirPath):
        # Calculate dirVecInit, dirVecFinal
        # Calculate point of intersection of dirVecInit & dirVecFinal
        # Calculate the angular bisector of dirVecInit & dirVecInit
        # Repeat the angular bisection, until the no. of bisectors >= noImages
        unitVecInit, magInit = interstitial.get_vec(crystalStrucInit, neighborIndex, intstIndex)
        unitVecFinal, magFinal = interstitial.get_vec(crystalStrucFinal, neighborIndex, intstIndex)

        intersection_pt = crystalStrucInit[neighborIndex].coords #NEEDS to be removed
        distance = 0.96
        #intersection_pt =  get_pt_of_intersec(crystalStrucInit[neighborIndex].coords, dirVecInit,
        #                                      crystalStrucFinal[neighborIndex].coords, dirVecFinal)
        bisectorLine = unitVecInit + unitVecFinal
        unitBisector = bisectorLine/np.linalg.norm(bisectorLine)
        intstCoords = crystalStruc[neighborIndex].coords + distance*np.array(dirVec)
                                              
                                              
    def get_pt_of_intersec(self, point1, vec1, point2, vec2):
        return "Not yet implemented"
        
    def remove_interstitial(self, crystalStrucInit, crystalStrucFinal,
			    intstIndex = None, intstType = None):
	self.crystalStrucInit = crystalStrucInit
	self.crystalStrucFinal = crystalStrucFinal
	if (intstType is not None and intstIndex is None):
	    self.crystalStrucInit.remove_species(intstType)
	    self.crystalStrucFinal.remove_species(intstType)
	elif (intstIndex is not None and intstType is None):
	    self.crystalStrucInit.remove_sites(intstType)
	    self.crystalStrucFinal.remove_sites(intstType)
	else:
	    raise RuntimeError('intstIndex and intstType cannot
			       simulatenously be None or have values')
    

