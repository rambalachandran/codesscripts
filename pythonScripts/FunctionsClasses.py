
# coding: utf-8

# In[ ]:

"""
Functions and classes outline (which upon maturing would be made into standalone modules)
You can convert them to .py files employing the follwing command on cmdline

$ ipython nbconvert --to python FunctionsClasses.ipynb

"""

import os, sys
from pprint import pprint
import numpy as np

import pymatgen as mg
from pymatgen.core.structure import Structure
from pymatgen.io.vasp import Poscar

class interstitial:
    def create_interstitial(self,crystalStruc,atomName,neighborIndex,dirVal,distance):
	if isinstance(dirVal,int):
	    dirVec = self.get_unitvec(crystalStruc,neighborIndex,dirVal)
	elif (instance(dirVal,list) and len(dirVal) == 3) :
	    dirVec = dirVal
	else:
	    raise RuntimeError('dirVal must be an index or a vector')	    
	intstCoords = crystalStruc[neighborIndex].coords + distance*np.array(dirVec)
	crystalStruc.append(atomName,intstCoords,True)

    def get_unitvec(self,crystalStruc, initIndex, finalIndex):
	""" Calculates the unitvector between the final & initial index atoms
        """
	coord1 = crystalStruc[initIndex].coords
	coord2 = crystalStruc[finalIndex].coords
	diffVec = np.asarray(coord2) - np.asarray(coord1)
	unitDiffVec = diffVec/np.linalg.norm(diffVec)
	return unitDiffVec

class fileOperations:
    def get_subdirs(self,folderPath,mindepth,maxdepth,startswith=None,endsWith=None,type=None):	
	return "To be Implemented"

    def get_neb_subdirs(self,folderPath,noImages):
	return "To be Implemented"

"""
TEST classes & Functions
"""
class Complex:
    def __init__(self,realpart,imagpart):
	self.r = realpart + 1
	self.i = imagpart + 1

				    

