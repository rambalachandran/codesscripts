import os, sys
from pprint import pprint
import numpy as np
import pymatgen as mg
from pymatgen.core.structure import Structure
from pymatgen.io.vaspio import Poscar

class interstitial:
    def create_interstitial(self,crystalStruc,atomName,neighborIndex,dirVal,distance):
	if isinstance(dirVal,int):
	    dirVec = self.get_vec(crystalStruc,neighborIndex,dirVal)
	elif (instance(dirVal,list) and len(dirVal) == 3) :
	    dirVec = dirVal
	else:
	    raise RuntimeError('dirVal must be an index or a vector')
        
	intstCoords = crystalStruc[neighborIndex].coords + distance*np.array(dirVec)
	crystalStruc.append(atomName,intstCoords,True)

    def get_vec(self,crystalStruc, initIndex, finalIndex):
	"""
        Calculates the unitvector between the final & initial index atoms
        """
	coord1 = crystalStruc[initIndex].coords
	coord2 = crystalStruc[finalIndex].coords
	diffVec = np.asarray(coord2) - np.asarray(coord1)
	unitDiffVec = diffVec/np.linalg.norm(diffVec)
	return unitDiffVec
