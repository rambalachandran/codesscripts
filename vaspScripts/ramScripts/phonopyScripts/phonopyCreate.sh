#!/bin/bash
# argument1 - no. of poscars
nMin=$1
nMax=$2
#fileName=$2
#folderName=$3

for i in $(seq -w $nMin $nMax); do
    mkdir disp-$i ;
    mv -v "POSCAR-${i}" "disp-${i}/POSCAR" ;
    cp "INCAR" "disp-${i}/INCAR"
    cp "POTCAR" "disp-${i}/POTCAR"
    cp "KPOINTS" "disp-${i}/KPOINTS"
    cp "run.pbs" "disp-${i}/run.pbs"
done

for i in $(seq -w $nMin $nMax); do
    cd disp-$i ;
    qsub run.pbs ;
    cd - ;
done
