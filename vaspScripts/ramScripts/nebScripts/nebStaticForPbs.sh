#!/bin/bash
#argument1 - nebFolderPath
#argument2 - outFolderPath
#argument3 - noImages
#argument4 - incarFilePath
#argument5 - kpointsFilePath
#argument6 - pbsFilePath

read -p 'Neb Folder Path: ' nebFolderPath
read -p 'Output Folder Path: ' outFolder
read -p 'No. Of Images: ' noImages
read -p 'Incar File Path: ' incarFile
read -p 'Kpoints File Path: ' kpointsFile
read -p 'PBS File Path: ' pbsFile

#nebFolderPath=$1
#outFolder=$2
#noImages=$3
#incarFile=$4
#kpointsFile=$5
#pbsFile=$6

echo 'NEB FOLDER:' $nebFolderPath
echo 'NO: IMAGES:' $noImages
echo 'INCAR FILE PATH:' $incarFile
echo 'KPOINTS FILE PATH:' $kpointsFile
echo 'NEB STATIC FOLDER:' $outFolder

nMin=00
tmpVal=01
nMax=$(( noImages + tmpVal ))

echo $nMin $nMax

cd $outFolder
for i in $(seq -w $nMin $nMax); do
    echo $nebFolderPath/$i/CONTCAR ;
    mkdir $i ;
    cp $nebFolderPath/$i/CONTCAR $i/POSCAR ;
    cp $nebFolderPath/POTCAR $i/POTCAR ;
    cp $incarFile $i/INCAR ;
    cp $kpointsFile $i/KPOINTS ;
    cp $pbsFile $i/run.pbs ;
    cd $i ;
    qsub run.pbs ;
    cd - ;
done
  
    
