#!/bin/bash
#argument1 - nebFolderPath
#argument2 - noImages
#echo 'Give nebPath, noImages,'
#read nebFolderPath noImages <<< $1 $2
nebFolderPath=$1
noImages=$2
echo 'FOLDER PATH:' $nebFolderPath
echo 'NO. IMAGES:' $noImages
#echo 'RESTART NO:' $restartNo

restartNo=01
while [ -e OUTCAR_${restartNo} ] || [ -e OUTCAR_${restartNo}.gz ]; do
    echo Found OUTCAR_${restartNo}
    m=`expr $restartNo + 1`
    if [ $m -le 9 ];then
	restartNo=0${m}
    else
	restartNo=${m}
    fi
done

nMin=00
tmpVal=01
nMax=$(( noImages + tmpVal ))

cd $nebFolderPath
for i in $(seq -w $nMin $nMax); do
    cp $i/POSCAR $i/POSCAR_$restartNo ;
    cp $i/OUTCAR $i/OUTCAR_$restartNo ;
    cp $i/EIGENVAL $i/EIGENVAL_$restartNo ;
    cp $i/OSZICAR $i/OSZICAR_$restartNo ;
    cp $i/XDATCAR $i/XDATCAR_$restartNo ;
    cp $i/CONTCAR $i/POSCAR ;
done

cp vasprun.xml vasprun_$restartNo.xml
cp stdout stdout_$restartNo

#qsub run.pbs
