#!/bin/bash
#argument1 - nebFolderPath
#argument2 - noImages
echo 'Give nebPath, noImages, restartNo:'
read nebFolderPath noImages restartNo
echo 'FOLDER PATH:' $nebFolderPath
echo 'NO. IMAGES:' $noImages
echo 'RESTART NO:' $restartNo

nMin=00
tmpVal=01
nMax=$(( noImages + tmpVal ))

cd $nebFolderPath
for i in $(seq -w $nMin $nMax); do
    cp $i/POSCAR $i/POSCAR_$restartNo ;
    cp $i/OUTCAR $i/OUTCAR_$restartNo ;
    cp $i/EIGENVAL $i/EIGENVAL_$restartNo ;
    cp $i/OSZICAR $i/OSZICAR_$restartNo ;
    cp $i/XDATCAR $i/XDATCAR_$restartNo ;
    cp $i/CONTCAR $i/POSCAR ;
done

cp vasprun.xml vasprun_$restartNo.xml
qsub run.pbs
