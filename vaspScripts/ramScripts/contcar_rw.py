import os, sys
from pprint import pprint
import numpy as np

import pymatgen as mg
from pymatgen.core.structure import Structure
from pymatgen.io.vaspio import Poscar

inPath = raw_input ("Input Path: ")
outPath = raw_input ("Output Path: ")

contcarPath = os.path.join(inPath, 'CONTCAR')
poscarPath = os.path.join(outPath, 'POSCAR')

print "CONTCAR PATH: ", contcarPath
print "POSCAR PATH: ", poscarPath

crystalStruc = Structure.from_file(contcarPath)
poscarFile = Poscar(crystalStruc, "BZO-ICSD")
poscarFile.write_file(poscarPath)

print "Job Completed"

