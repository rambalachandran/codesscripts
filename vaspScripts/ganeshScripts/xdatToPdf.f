      implicit none
      real xi,yi,zi,avol,dt
      real x(10000,9),y(10000,9),z(10000,9)
      integer ntype,numb(9),switch,nblocks
      character*1 line
      common/data/x,y,z,ntype,numb,switch,nblocks,line
      real dij,ex,ey,ez,dx,dy,dz
      integer i,j,la,lb,lc,isep,idr,nb,nt
      integer hist(0:1500,9,9),hsize
      real cutoff,sigma,rho,pdf,r,g,volume,Gofr,angst
      integer alim,blim,clim,ntot,showlim
      real gauss
      integer t1,t2,filenumber(9,9),N12(9,9)
      integer mult
      read(*,*) switch,ntype,(numb(nt),nt=1,ntype)
      if(ntype.gt.9) then
         write(*,*) 'Error. Too many types ',ntype
         stop
      endif
c      write(*,*) switch,ntype
      nblocks=0
c      call flush(6)
c      read(*,*) 
c      write(*,*) line
c      call flush(6)
      read(*,*) avol,ex,ey,ez,dt
c      write(*,*) avol,ex,ey,ez,dt
c      call flush(6)
c      read(*,*) 
c      read(*,*) 
c      read(*,*) 
      angst=1.0E-10
      ex=ex/angst
      ey=ey/angst
      ez=ez/angst
      showlim=10.0
      cutoff=showlim+2.0
      sigma=0.05
      hsize=100.0*cutoff
      alim=1+cutoff/ex
      blim=1+cutoff/ey
      clim=1+cutoff/ez
      ntot=0
      do i=1,ntype
         ntot=ntot+numb(i)
      enddo
      volume=ex*ey*ez
      rho=float(ntot)/volume
c      write(*,*) ex,ey,ez,volume,alim,blim,clim
c      alim=0
c      blim=0
c      clim=0
      do t1=1,ntype
         do t2=1,ntype
            filenumber(t1,t2)=10*t1+t2
            N12(t1,t2)=numb(t1)*numb(t2)
c            write(*,*) t1,t2,filenumber(t1,t2),N12(t1,t2)
            do isep=0,hsize
               hist(isep,t1,t2)=0
            enddo
         enddo
      enddo

      do nb=1,1000000
         read(*,*,end=101)
         nblocks=nblocks+1
         do nt=1,ntype
            do i=1,numb(nt)
               read(*,*) xi,yi,zi
               x(i,nt)=xi*ex
               y(i,nt)=yi*ey
               z(i,nt)=zi*ez
            enddo
         enddo
c         write(*,*) 'read in block ',nb,x(1,1)/ex,y(1,1)/ey,z(1,1)/ez
         do t1=1,ntype
            do t2=1,ntype

               do la=-alim,alim
                  do lb=-blim,blim
                     do lc=-clim,clim
                        dx=la*ex
                        dy=lb*ey
                        dz=lc*ez
                        do i=1,numb(t1)
                           do j=1,numb(t2)
            if((t1.eq.t2).and.(j.eq.i)) then
               if((la.eq.0).and.(lb.eq.0).and.(lc.eq.0)) goto 100
            endif
            dij=(dx+x(i,t1)-x(j,t2))**2
            dij=dij+(dy+y(i,t1)-y(j,t2))**2
            dij=sqrt(dij+(dz+z(i,t1)-z(j,t2))**2)
            isep=100.0*dij
            if(isep.le.hsize) hist(isep,t1,t2)=hist(isep,t1,t2)+1
c            write(*,*) la,lb,lc,i,j,dij,isep,hist(isep)
 100        continue
                           enddo
                        enddo
                     enddo
                  enddo
               enddo

            enddo
         enddo

      enddo

 101  continue
c      write(*,*) 'Read ',nblocks,' blocks'
c      write(*,*) 'of ',(numb(nt),nt=1,ntype),' atoms'

      do t1=1,ntype
         do t2=1,ntype

      mult=4
      do idr=1,mult*hsize
         r=float(idr)/100.0/float(mult)
         pdf=0.0
c         write(*,*) r,hist(idr)
         do isep=0,hsize
            g=gauss((r-(0.5+float(isep))/100.0)/(sigma))/sigma
            pdf=pdf+float(hist(isep,t1,t2))*g/float(nblocks)
         enddo
         pdf=volume*pdf/(4.0*3.141592654*float(N12(t1,t2))*r*r)
         Gofr=4.0*3.141592654*r*rho*(pdf-1.0)
         if(switch.eq.1) then
            if(r.le.showlim) write(filenumber(t1,t2),900) r,pdf
         elseif(switch.eq.2) then
            if(r.le.showlim) write(filenumber(t1,t2),900) r,Gofr
         endif
      enddo
c      write(filenumber(t1,t2),*) ''
      enddo
      enddo
      stop
 900  format(2f12.6)
      end

      real function gauss(x)
      real x
      gauss=(1.0/sqrt(2.0*3.141592654))*exp(-x*x/2.0)
      return
      end
