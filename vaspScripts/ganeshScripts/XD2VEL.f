      Implicit None
      
      Real*8 alat(3,3),r1v(1000,10000,3),vel(3),dt,redc(3)
      Integer*4 i,nat,nb,natoms,nblocks,numbt(9),ntype,ntrans,minb
      character char(9)
      
      ntype=2
      dt=0.5d0
      Read(*,*)ntrans
      Read(*,*)
      Read(*,*)
      Read(*,*)(alat(1,i),i=1,3)
      Read(*,*)(alat(2,i),i=1,3)
      Read(*,*)(alat(3,i),i=1,3)
      Read(*,*)(char(i),i=1,ntype)
      Read(*,*)(numbt(i),i=1,ntype)
      natoms=0
      Do i=1,ntype
         natoms=natoms+numbt(i)
      enddo
      If(natoms.gt.1000) stop
c      write(*,*)natoms
      nblocks=0
      Do nb=1,1000000
         If (nb.gt.10000) stop
         Read(*,*,end=1)
         nblocks=nblocks+1
         Do nat=1,natoms
            Read(*,*)(r1v(nat,nb,i),i=1,3)
         enddo
      enddo
 1    continue
c     
      if(ntrans.ge.1) then
         minb=ntrans
      else
         minb=1
      endif
c     In VASP the vel(nth-step)=(R(n+1)th-step)-(R(nth-step))/dt  which is consistent with VELOCITY in OUTCAR    
      Do nb=minb,nblocks-1
         write(*,*)
         Do nat=1,natoms
            if(nat.eq.9.and.nb.eq.2) then
               vel(1)=r1v(nat,nb,1)*alat(1,1)+r1v(nat,nb,2)*alat(2,1)+r1v(nat,nb,3)*alat(3,1)
               vel(2)=r1v(nat,nb,1)*alat(1,2)+r1v(nat,nb,2)*alat(2,2)+r1v(nat,nb,3)*alat(3,2) 
               vel(3)=r1v(nat,nb,1)*alat(1,3)+r1v(nat,nb,2)*alat(2,3)+r1v(nat,nb,3)*alat(3,3)
               write(10,*) "nb", (vel(i),i=1,3)
               vel(1)=r1v(nat,nb+1,1)*alat(1,1)+r1v(nat,nb+1,2)*alat(2,1)+r1v(nat,nb+1,3)*alat(3,1)
               vel(2)=r1v(nat,nb+1,1)*alat(1,2)+r1v(nat,nb+1,2)*alat(2,2)+r1v(nat,nb+1,3)*alat(3,2) 
               vel(3)=r1v(nat,nb+1,1)*alat(1,3)+r1v(nat,nb+1,2)*alat(2,3)+r1v(nat,nb+1,3)*alat(3,3)
               write(10,*) "nb+1", (vel(i),i=1,3)
            endif

            redc(1)=(r1v(nat,nb+1,1)-r1v(nat,nb,1))
            redc(2)=(r1v(nat,nb+1,2)-r1v(nat,nb,2))
            redc(3)=(r1v(nat,nb+1,3)-r1v(nat,nb,3))
            if(nat.eq.9.and.nb.eq.2) write(10,*) (redc(i),i=1,3)
            call intocell(redc)
            if(nat.eq.9.and.nb.eq.2) write(10,*) (redc(i),i=1,3)
            vel(1)=redc(1)*alat(1,1)+redc(2)*alat(2,1)+redc(3)*alat(3,1)
            vel(2)=redc(1)*alat(1,2)+redc(2)*alat(2,2)+redc(3)*alat(3,2) 
            vel(3)=redc(1)*alat(1,3)+redc(2)*alat(2,3)+redc(3)*alat(3,3)
            if(nat.eq.9.and.nb.eq.2) write(10,*) (vel(i),i=1,3)
            Do i=1,3
               vel(i)=vel(i)/dt
            enddo
            if(nat.eq.9.and.nb.eq.2) write(10,*) (vel(i),i=1,3)
            write(*,*)(vel(i),i=1,3)
         enddo
      enddo
      stop
      end
      
      subroutine intocell(v)
      Implicit None
      real*8 v(3)
      if(v(1).gt.0.5)then 
         v(1)=v(1)-1
      elseif(v(1).le.-0.5)then 
         v(1)=v(1)+1
      endif
      if(v(2).gt.0.5)then 
         v(2)=v(2)-1
      elseif(v(2).le.-0.5)then 
         v(2)=v(2)+1
      endif
      if(v(3).gt.0.5)then 
         v(3)=v(3)-1
      elseif(v(3).le.-0.5) then 
         v(3)=v(3)+1
      endif
      return
      end

         
