	Implicit None
	Real*8 x,y,z,vel(10000,3)
	Integer*4 ntype,i,j,k,numb(9),countt,maxcount
	Integer*4 natoms,nmoli(3),nb,vari,moli(10000,3)
	character*3 h2id1,h2id2,molc(10000)	
	
	open (10,file="poscar_temp")
!	Read(*,*) id1,id2,ntype
	ntype=3
!       moli(x,y): y=1, H(H2), y=2 H(H2S),y=3 S
	Do i=1,3
	   nmoli(i)=0
	enddo
	Read(10,*)	
	Read(10,*)
	Read(10,*)
	Read(10,*)
	Read(10,*)
	Read(10,*) 
	Read(10,*) (numb(i),i=1,2)
	write(*,*) (numb(i),i=1,2)
	Read(10,*)
	natoms=0	
	Do i=1,2
		natoms=natoms+numb(i)
	enddo
	countt=0
	Do i=1,10000
	   Read(10,*,end=1)x,y,z,vari,vari,molc(i)
	   
	   if(molc(i).eq."Hyd") then
	      write(*,*)molc(i),"H-H2",i
	      nmoli(1)=nmoli(1)+1
	      moli(nmoli(1),1)=i
	   elseif(i.le.numb(1))then
	      write(*,*)molc(i),"H-H2S",i
	      nmoli(2)=nmoli(2)+1
	      moli(nmoli(2),2)=i
	   else
	      write(*,*)molc(i),"S",i
	      nmoli(3)=nmoli(3)+1
	      moli(nmoli(3),3)=i
	   endif
	enddo
 1	continue
	write(*,*) "Number of"," H-H2",nmoli(1)," H-H2S",nmoli(2)," S",nmoli(3)
	Do nb=1,100000000
	   Read(*,*,end=2)
	   Do i=1,natoms 		
	      Read(*,*)(vel(i,j),j=1,3)
	   enddo
	   write(252,*)
	   Do i=1,3
	      Do j=1,nmoli(i)
		 write(252,*) (vel(moli(j,i),k),k=1,3)
	      enddo
	   enddo
	enddo
 2	continue
	stop
	end
