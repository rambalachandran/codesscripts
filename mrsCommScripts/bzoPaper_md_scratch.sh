#!/bin/bash

## Importing Other Scripts
source ./bashFunctionsForDFT.sh

## STEP1: To see if the OSZICAR was created in the correct order
step1()
{
    folderPath="/global/cscratch1/sd/rbala/Proj_m2113/Perovskites/BZO_Paper/abinitio_md_v0/YBZO-MD/BZO_Y2a_OH_Oindex120_88_relax/MD/2Y_2H/T_0900K/test"
    fileType="OSZICAR_"
    mergedFilePath="$folderPath/OSZICAR-merged"
    noFiles=25
    stringVal="T="
    totOccurence=0
    for i in $(seq -f "%02g" 1 $noFiles); do
	# echo $i, "$fileType$i"
	filePath="$folderPath/$fileType$i"
	noOccurence=$(grep $stringVal $filePath | wc -l)
	totOccurence=$((totOccurence+noOccurence))
	#searchNo=$((noOccurence  -20)
	locOccurence=$(awk -v N=$noOccurence -F " "  \
			   '/T=/ && !--N {print $1 " "$3 " "$5 ","; exit}' $filePath)
	   
	mergedOccurence=$(awk -v N=$totOccurence -F " "  \
			      '/T=/ && !--N {print $1 " "$3 " "$5 ","; exit}' $mergedFilePath)
	
	echo "$i, $noOccurence, $totOccurence $locOccurence $mergedOccurence"
    done
}


# To enable run the codes inside functions directly from terminal
# bash createApRun.sh func_name arg1 arg2 ... argN
"$@"
