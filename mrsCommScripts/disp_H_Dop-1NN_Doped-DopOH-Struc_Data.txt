Dopant-Type In
Total Dopant Index Dict {'In': [53]}
Total Dopant Index List [53]
distanceList[[135, 135, 0.14455715918915224]]
None
Max.Displacement (in Ang) 0.144557159189
Min.Dev Displacement (in Ang) 0.144557159189
Avg.Displacement (in Ang) 0.144557159189
Std.Dev Displacement (in Ang) 0.0
[135] 2.36432164586 2.36432164586



Dopant-Type Sm
Total Dopant Index Dict {'Sm': [53]}
Total Dopant Index List [53]
distanceList[[135, 135, 0.32276561084268185]]
None
Max.Displacement (in Ang) 0.322765610843
Min.Dev Displacement (in Ang) 0.322765610843
Avg.Displacement (in Ang) 0.322765610843
Std.Dev Displacement (in Ang) 0.0
[135] 2.43200449581 2.43200449581



Dopant-Type Er
Total Dopant Index Dict {'Er': [53]}
Total Dopant Index List [53]
distanceList[[135, 135, 0.18303999973516222]]
None
Max.Displacement (in Ang) 0.183039999735
Min.Dev Displacement (in Ang) 0.183039999735
Avg.Displacement (in Ang) 0.183039999735
Std.Dev Displacement (in Ang) 0.0
[135] 2.38683313519 2.38683313519



Dopant-Type Y
Total Dopant Index Dict {'Y': [53]}
Total Dopant Index List [53]
distanceList[[135, 135, 0.3189816080893598]]
None
Max.Displacement (in Ang) 0.318981608089
Min.Dev Displacement (in Ang) 0.318981608089
Avg.Displacement (in Ang) 0.318981608089
Std.Dev Displacement (in Ang) 0.0
[135] 2.39640581886 2.39640581886



Dopant-Type Ho
Total Dopant Index Dict {'Ho': [53, 135]}
Total Dopant Index List [53, 135]
distanceList[[135, 135, 0.36727042984277136]]
None
Max.Displacement (in Ang) 0.367270429843
Min.Dev Displacement (in Ang) 0.367270429843
Avg.Displacement (in Ang) 0.367270429843
Std.Dev Displacement (in Ang) 0.0
[135] 2.39321683898 2.39321683898



distanceList[[135, 135, 0.36727042984277136]]
None
Max.Displacement (in Ang) 0.367270429843
Min.Dev Displacement (in Ang) 0.367270429843
Avg.Displacement (in Ang) 0.367270429843
Std.Dev Displacement (in Ang) 0.0
[135] 2.39321683898 2.39321683898



Dopant-Type Ga
Total Dopant Index Dict {'Ga': [53]}
Total Dopant Index List [53]
distanceList[[135, 135, 0.1892948464871541]]
None
Max.Displacement (in Ang) 0.189294846487
Min.Dev Displacement (in Ang) 0.189294846487
Avg.Displacement (in Ang) 0.189294846487
Std.Dev Displacement (in Ang) 0.0
[135] 2.03915901447 2.03915901447



Dopant-Type Dy
Total Dopant Index Dict {'Dy': [53]}
Total Dopant Index List [53]
distanceList[[135, 135, 0.36800210165464875]]
None
Max.Displacement (in Ang) 0.368002101655
Min.Dev Displacement (in Ang) 0.368002101655
Avg.Displacement (in Ang) 0.368002101655
Std.Dev Displacement (in Ang) 0.0
[135] 2.3998939836 2.3998939836



Dopant-Type Pm
Total Dopant Index Dict {'Pm': [53]}
Total Dopant Index List [53]
distanceList[[135, 135, 0.41913782137562955]]
None
Max.Displacement (in Ang) 0.419137821376
Min.Dev Displacement (in Ang) 0.419137821376
Avg.Displacement (in Ang) 0.419137821376
Std.Dev Displacement (in Ang) 0.0
[135] 2.43912232625 2.43912232625



Dopant-Type Sc
Total Dopant Index Dict {'Sc': [53]}
Total Dopant Index List [53]
distanceList[[135, 135, 0.16399230294455613]]
None
Max.Displacement (in Ang) 0.163992302945
Min.Dev Displacement (in Ang) 0.163992302945
Avg.Displacement (in Ang) 0.163992302945
Std.Dev Displacement (in Ang) 0.0
[135] 2.11383312803 2.11383312803



Dopant-Type La
Total Dopant Index Dict {'La': [53]}
Total Dopant Index List [53]
distanceList[[135, 135, 0.47206505183348735]]
None
Max.Displacement (in Ang) 0.472065051833
Min.Dev Displacement (in Ang) 0.472065051833
Avg.Displacement (in Ang) 0.472065051833
Std.Dev Displacement (in Ang) 0.0
[135] 2.46420325312 2.46420325312



Dopant-Type Al
Total Dopant Index Dict {'Al': [53]}
Total Dopant Index List [53]
distanceList[[135, 135, 0.20326276779036181]]
None
Max.Displacement (in Ang) 0.20326276779
Min.Dev Displacement (in Ang) 0.20326276779
Avg.Displacement (in Ang) 0.20326276779
Std.Dev Displacement (in Ang) 0.0
[135] 1.99126815456 1.99126815456



Dopant-Type Gd
Total Dopant Index Dict {'Gd': [53]}
Total Dopant Index List [53]
distanceList[[135, 135, 0.34318105973174584]]
None
Max.Displacement (in Ang) 0.343181059732
Min.Dev Displacement (in Ang) 0.343181059732
Avg.Displacement (in Ang) 0.343181059732
Std.Dev Displacement (in Ang) 0.0
[135] 2.4141117066 2.4141117066



Dopant-Type Tl
Total Dopant Index Dict {'Tl': [53]}
Total Dopant Index List [53]
distanceList[[135, 135, 0.1365013689352717]]
None
Max.Displacement (in Ang) 0.136501368935
Min.Dev Displacement (in Ang) 0.136501368935
Avg.Displacement (in Ang) 0.136501368935
Std.Dev Displacement (in Ang) 0.0
[135] 2.406998304 2.406998304



Dopant-Type Nd
Total Dopant Index Dict {'Nd': [53]}
Total Dopant Index List [53]
distanceList[[135, 135, 0.4191392188181964]]
None
Max.Displacement (in Ang) 0.419139218818
Min.Dev Displacement (in Ang) 0.419139218818
Avg.Displacement (in Ang) 0.419139218818
Std.Dev Displacement (in Ang) 0.0
[135] 2.44900312645 2.44900312645



