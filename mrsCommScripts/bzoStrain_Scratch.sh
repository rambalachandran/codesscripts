
#!/bin/bash

## Importing Other Scripts
source ./bashFunctionsForDFT.sh

## STEP-1 - To copy Relax struc data from noStrain Folder to the other strained folders
step1()
{
    # ISSUE: Think how to use awk if min or maxDepth is greater than 1
    masterRootPath=/scratch1/scratchdirs/rbala/Proj_m2113/BZO_Strain/relax
    inRootPath=$masterRootPath/noStrain
    echo $inRootPath
    # outDirList=(tenStrain_3 tenStrain_5 tenStrain_7 tenStrain_10)
    outDirList=(tenStrain_15)
    strainType=1
    #outDirList=(compStrain_3 compStrain_5 compStrain_7 compStrain_10)
    #outDirList=(compStrain_15)
    #strainType=-1
    for outDirVal in "${outDirList[@]}"; do
	minDepth=2
	maxDepth=2
	outRootPath=$masterRootPath/$outDirVal
	if [ ! -d "$outRootPath" ]; then
	    mkdir -p $outRootPath
	fi
	strainVal=$(echo $outRootPath| awk -F "_" '{print $NF}')
	strainVal=$(bc <<< "scale=4; $strainVal/100")
	echo "Out Root Path is: $outRootPath"
	echo "Strain Value is $strainVal"
	for i in $(find $inRootPath -mindepth $minDepth \
			-maxdepth $maxDepth -type d | sort); do
	    subDir=$(echo $i|awk -v n=$maxDepth -F "/" '{for (--n; n >= 0; n--){ printf "%s/", $(NF-n)} print ""}')
	    finalDir=$(echo $subDir|awk -F "/" '{print $(NF-1)}')
	    echo $i
	    echo "SubDir is: $subDir"
	    echo "finalDir is: $finalDir"
	    if [ $finalDir != "old" ]; then	    
		inPath=$inRootPath/$subDir
		outPath=$outRootPath/$subDir
		echo "Infolder Path is : $inPath"
		echo "Outfolder Path is : $outPath"
		if [ ! -d "$outPath" ]; then
	    	    mkdir -p $outPath
		fi
		copy_vasp_input $inPath $outPath
		change_cubicposcar_vol $outPath/POSCAR $strainVal $strainType
	    fi
	done
    done
}


## STEP-1b - To copy Relax struc data from noStrain Folder to the other strained folders and create epitaxial strain in only X Y direction

step1b()
{
    # ISSUE: Think how to use awk if min or maxDepth is greater than 1
    inRootPath=/scratch1/scratchdirs/rbala/Proj_m2113/BZO_Strain/relax/noStrain
    outMasterRootPath=/scratch1/scratchdirs/rbala/Proj_m2113/BZO_Strain/relax/epitaxialStrain_Isif3
    isifVal=3
    strainDir="Epitaxy"
    includeDirList=("/BZO/")
    exemptDirList=("/old")
    echo $inRootPath
    # outDirList=(tenStrain_3 tenStrain_5 tenStrain_7 tenStrain_10 tenStrain_15)
    ## outDirList=(tenStrain_3)
    # strainType=1
    outDirList=(compStrain_3 compStrain_5 compStrain_7 compStrain_10 compStrain_15)
    # outDirList=(compStrain_15)
    strainType=-1
    for outDirVal in "${outDirList[@]}"; do
	minDepth=2
	maxDepth=2
	outRootPath=$outMasterRootPath/$outDirVal
	if [ ! -d "$outRootPath" ]; then
	    mkdir -p $outRootPath
	fi
	strainVal=$(echo $outRootPath| awk -F "_" '{print $NF}')
	strainVal=$(bc <<< "scale=4; $strainVal/100")
	echo "Out Root Path is: $outRootPath"
	echo "Strain Value is $strainVal"
	for i in $(find $inRootPath -mindepth $minDepth \
			-maxdepth $maxDepth -type d | sort); do
	    subDir=$(echo $i|awk -v n=$maxDepth -F "/" '{for (--n; n >= 0; n--){ printf "%s/", $(NF-n)} print ""}')
	    finalDir=$(echo $subDir|awk -F "/" '{print $(NF-1)}')
	    echo $i
	    echo "SubDir is: $subDir"
	    echo "finalDir is: $finalDir"
	    for includeDir  in "${includeDirList[@]}"; do
		if [ ! "${i/$includeDir}" = "$i" ] ; then
		    for exemptDir  in "${exemptDirList[@]}"; do
			if [ "${i/$exemptDir}" = "$i" ] ; then
			    inPath=$inRootPath/$subDir
			    outPath=$outRootPath/$subDir
			    echo "Infolder Path is : $inPath"
			    echo "Outfolder Path is : $outPath"
			    if [ ! -d "$outPath" ]; then
	    			mkdir -p $outPath
			    fi
			    copy_vasp_input $inPath $outPath
			    change_cubicposcar_vol $outPath/POSCAR  \
						   $strainVal $strainType $strainDir
			    # Modify INCAR
			    sed -i '/ISIF/{h;s/.*/ISIF='"$isifVal"'/};${x;/^$/{s//ISIF='"$isifVal"'/;H};x}' $outPath/INCAR
			fi
		    done
		fi
	    done
	done	    
    done
}

##STEP2 Identify the number of folders we have to run jobs
step2()
{
    rootPath=/scratch1/scratchdirs/rbala/Proj_m2113/BZO_Strain/relax/epitaxialStrain_Isif3
    exemptDirList=("/noStrain" "/old" "/miscTempTestOldOther")
    minDepth=3
    maxDepth=3
    # rootPath=/scratch1/scratchdirs/rbala/Proj_m2113/BZO_Strain/relax/miscTempTestOldOther
    # exemptDirList=("/noStrain" "/old" )
    # minDepth=3
    # maxDepth=3

    echo "Root Path is: $rootPath"
    count_num_subdirs $rootPath $minDepth $maxDepth $echoMode $exemptDirList
}


##STEP3 Create jobscript for the epitaxial strain pure structure jobs
step3()
{
    rootPath="/scratch1/scratchdirs/rbala/Proj_m2113/BZO_Strain/relax/epitaxialStrain"
    read minDepth maxDepth <<<$(echo 1 4)
    includeDirList="/"
    exemptDirList=("noStrain" "old" "miscTempTestOldOther")
    modulelist=('vasp' 'python')
    read  maxJobsPerRun noNodesPerJob walltime queue premium account binaryName  \
	  <<<$(echo 90 8 "06:00:00" "regular" 0 "m2113" "vasp")
    echo "PRE FUNCTION CALL"
    echo  "$rootPath $minDepth $maxDepth"
    echo  "${includeDirList[@]} ${exemptDirList[@]} ${modulelist[@]}"
    echo  "$maxJobsPerRun $noNodesPerJob $walltime $queue $premium $account $binaryName"
    write_vaspjob_slurm $rootPath $minDepth $maxDepth  \
			includeDirList[@] exemptDirList[@]  modulelist[@]  \
			$maxJobsPerRun $noNodesPerJob $walltime $queue  \
			$premium $account $binaryName
}

# STEP-4 backup data to restart calculation and create jobscript
step4()
{
    rootPath="/scratch1/scratchdirs/rbala/Proj_m2113/BZO_Strain/relax/homogeneousStrain"
    strVal="accuracy"
    includeDirList="/"
    exemptDirList1Temp=($(grep -rl --include "stdout" $strVal $rootPath ))
    exemptDirList1=()
    for exemptVal in "${exemptDirList1Temp[@]}"; do
	exemptDirList1+=($(dirname "${exemptVal}"))
    done
    exemptDirList2=("noStrain" "old" "miscTempTestOldOther")
    declare -a exemptDirList=("${exemptDirList1[@]}" "${exemptDirList1[@]}")
    echo "##### DIRLIST START #####\n\n"
    printf '%s\n' "${exemptDirList1[@]}"    
    printf "##### DIRLIST END #####\n\n"
    
    printf "##### VASP FILEBACKUP BEGIN #####\n\n"
    read minDepth maxDepth <<<$(echo 1 4)
    read  exemptDirCompare includeDirCompare neb noImages \
	  tarzip <<<$(echo  "Absolute" "Partial" "False" "0" "False")
    vasp_backup_for_restart $rootPath $minDepth $maxDepth  \
    			    includeDirList[@] exemptDirList[@] \
    			    $exemptDirCompare $includeDirCompare \
			    $neb $noImages $tarzip
    printf "##### VASP FILEBACKUP END #####\n\n"
    
    printf "##### VASP JOBSCRIPT BEGIN #####\n\n"
    modulelist=('vasp' 'python')
    read  maxJobsPerRun noNodesPerJob walltime queue premium account binaryName  \
	  <<<$(echo 90 8 "04:00:00" "regular" 0 "m2113" "vasp")
    write_vaspjob_slurm $rootPath $minDepth $maxDepth  \
			includeDirList[@] exemptDirList[@]  modulelist[@]  \
			$exemptDirCompare $includeDirCompare $maxJobsPerRun \
			$noNodesPerJob $walltime $queue  \
			$premium $account $binaryName
    printf "##### VASP JOBSCRIPT END #####\n\n"
}

# STEP5 - Identify discrepancy between new and old jobs
step5()
{
    dirPath="/scratch1/scratchdirs/rbala/Proj_m2113/miscOldTempTestData/compareJobs"
    fileName="runTot_Old_OnlyDir.job"
    maxDepth=7
    fileData=($(cat $dirPath/$fileName))
    for lineVal in "${fileData[@]}"; do
	echo $lineVal | awk -v n=$maxDepth -F "/" '{for (--n; n >= 0; n--){ printf "%s/", $(NF-n)} print ""}'  >>$dirPath/testOutput
    done
    ## MISSING DIRS
    # homogeneousStrain/compStrain_5/Y1_OH_Oindex120/EDIFF_1E-06_EDIFFG_1E-04_CHG_P1
    # homogeneousStrain/compStrain_5/Y1_OH_Oindex93/EDIFF_1E-06_EDIFFG_1E-04_CHG_P1
    # homogeneousStrain/compStrain_5/Y1_vac_Oindex120/EDIFF_1E-06_EDIFFG_1E-04_CHG_P2
}


# STEP-6 backup data to restart calculation and create jobscript only for systems whose forces are more than 0.01
step6()
{
    rootPath="/scratch1/scratchdirs/rbala/Proj_m2113/miscOldTempTestData/homogeneousStrain"    
    forceMin=0.01
    read minDepth maxDepth <<<$(echo 1 4)
    includeDirList=("/")
    exemptDirList1=()
    for i in $(find $rootPath -mindepth $minDepth \
		    -maxdepth $maxDepth -type d | sort); do
	if [ -f "$i/stdout" ]; then
	    forceVal=$(grep "g(F)" $i/stdout | tail -1 |
			      awk -F " " '{print $5}')
	    if [ ! -z "$forceVal" ]; then
		forceConvg=$(awk -v a="$forceMin" -v b="$forceVal" \
				 'BEGIN{print(a>b)}')
		if [ $forceConvg -eq 1 ]; then		
		    # printf "####### Folder Converged ####### \n"
		    printf "Folder Path: $i \n"
		    printf "  Force Val: $forceVal\n"
		    exemptDirList1+=($i)
		fi
	    else
		printf "***EMPTY FORCE VAL***"
		printf "Folder Path: $i \n"
	    fi
	fi
    done
    exemptDirList2=("noStrain" "old" "miscTempTestOldOther")
    declare -a exemptDirList=("${exemptDirList1[@]}" "${exemptDirList1[@]}")
    echo "##### DIRLIST START #####\n\n"
    printf '%s\n' "${exemptDirList[@]}"    
    printf "##### DIRLIST END #####\n\n"
    
    printf "##### VASP FILEBACKUP BEGIN #####\n\n"
    read  exemptDirCompare includeDirCompare neb noImages \
    	  tarzip <<<$(echo  "Absolute" "Partial" "False" "0" "False")
    vasp_backup_for_restart $rootPath $minDepth $maxDepth  \
    			    includeDirList[@] exemptDirList[@] \
    			    $exemptDirCompare $includeDirCompare \
    			    $neb $noImages $tarzip
    printf "##### VASP FILEBACKUP END #####\n\n"
    
    printf "##### VASP JOBSCRIPT BEGIN #####\n\n"
    modulelist=('vasp' 'python')
    read  maxJobsPerRun noNodesPerJob walltime queue premium account binaryName  \
    	  <<<$(echo 90 8 "04:00:00" "regular" 0 "m2113" "vasp")
    write_vaspjob_slurm $rootPath $minDepth $maxDepth  \
    			includeDirList[@] exemptDirList[@]  modulelist[@]  \
    			$exemptDirCompare $includeDirCompare $maxJobsPerRun \
    			$noNodesPerJob $walltime $queue  \
    			$premium $account $binaryName
    printf "##### VASP JOBSCRIPT END #####\n\n"
}

## PRINT STEPVALUE 
step7()
{
    filePath="/scratch1/scratchdirs/rbala/Proj_m2113/miscOldTempTestData/homogeneousStrain/run1.job"
    forceMax=0.01
    for i in $(grep "cd" $filePath| awk -F ' ' '{print $2}'); do
    	stepVal=$(grep "g(F)" $i/stdout | tail -1 | awk -F ' ' '{print $NF}')
	stepVal2=${stepVal:(-1)}
	stepVal=${stepVal/$stepVal2}	
	potimInput=$(grep "POTIM" $i/OUTCAR_3 | \
			    awk -F ' ' '{print $3}')
	potimOutput=$( echo $stepVal*$potimInput | bc)
	echo "$stepVal $potimInput $potimOutput"	
    done
}


##SCRATCH CODE For Testing
scratch_code_1()
{
    echo "TO BE IMPLEMENTED"
}
scratch_code_strain()
{
    echo "TO BE IMPLEMENTED" 
    # BLOCK 0
   # filePath="/scratch1/scratchdirs/rbala/Proj_m2113/miscOldTempTestData/homogeneousStrain/compStrain_10/BZO_OH_Oindex93/EDIFF_1E-06_EDIFFG_1E-04/stdout"
  #   forceValFile=$(grep "g(F)" $filePath | tail -1 |
  # 		     awk -F " " '{print $5}')
  #   forceVal=0.502E-01
  #   forceMin=0.01
  #   echo $forceMin'>'$forceVal | bc -l
  #   echo $forceMin'<'$forceVal | bc -l

  #   echo $forceValFile $forceMin 
  #   echo "$forceMin > $forceValFile" | bc -l
  #   echo "$forceMin < $forceValFile" | bc -l

    
    #echo $num1'<'$num2 | bc -l

    # min=0.01
    # val=0.737E-01
    # if [ 1 -eq "$(echo "${val} < ${min}" | bc -l)" ]; then
    # 	min=${val}
    # fi
    # echo "$min"

    #   ## BLOCK 1
 # my_string="/scratch1/scratchdirs/rbala/Proj_m2113/BZO_Strain/relax/epitaxialStrain_Isif3"
 #    #my_string="abdrcdgale"
 #    substringList=("/")
 #    for substring in "${substringList[@]}"; do
 # 	echo "$substring"
 # 	if [ "${my_string/$substring}" = "$my_string" ] ; then
 # 	    echo "${substring} is not in ${my_string}"
 # 	else
 # 	    echo "${substring} was found in ${my_string}"
 # 	fi
 #    done
 #    ## BLOCK 2
 #    rootPath="/scratch1/scratchdirs/rbala/Proj_m2113/BZO_Strain/relax"
 #    maxDepth=1
 #    for i in $(find $rootPath -maxdepth $maxDepth -name "*.job"   \
 # 		    -type f |sort); do
 # 	printf "FILENAME: $i \n\n"
 # 	grep "old" $i
 # 	grep "noStrain" $i
 # 	grep "cd" $i | wc -l
 #    done    

}



# To enable run the codes inside functions directly from terminal
# bash createApRun.sh func_name arg1 arg2 ... argN
"$@"
