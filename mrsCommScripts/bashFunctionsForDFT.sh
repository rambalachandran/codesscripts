#!/bin/bash
## SCRIPT TO ADD folders to wraprun in OLCF (TITAN)
##INPUT
write_vaspheader_slurm()
{
    read queue walltime account premium outFile   \
	 <<<$(echo  "$1" "$2" "$3" "$4" "$5")
    declare -a modulelist=("${!6}")

    echo "VASPHEADER SLURM FUNCTION CALL"
    echo  "$queue $walltime $account $premium $outFile"
    echo  "${modulelist[@]}"
    
    
    printf "#!/bin/bash\n" > $outFile
    printf "#SBATCH -p $queue\n" >> $outFile
    printf "#SBATCH -N \n" >> $outFile
    printf "#SBATCH --time=$walltime \n" >>$outFile
    printf "#SBATCH -A $account\n" >>$outFile
    printf "#SBATCH --mail-type=FAIL\n" >>$outFile

    if [ $premium -eq 1 ]; then
	printf "#SBATCH --qos=premium\n" >>$outFile
    fi
    printf "\n\n" >>$outFile
    printf "##LOAD MODULES\n" >>$outFile
    # SO-Link: http://unix.stackexchange.com/a/106085/77596
    for module in "${modulelist[@]}"; do
	printf "module load $module\n" >>$outFile
    done
    printf " \n \n" >>$outFile
}


write_vaspjob_slurm()
{
    rootPath=$1
    minDepth=$2
    maxDepth=$3
    if [ -z "$4" ]; then
	includeDirList=("/")
	exemptDirList=("")
	modulelist=('vasp' 'python')
    else
	declare -a includeDirList=("${!4}")
	declare -a exemptDirList=("${!5}")
	declare -a modulelist=("${!6}")
    fi    
    if [ -z "$7" ]; then
	read exemptDirCompare includeDirCompare maxJobsPerRun \
	     noNodesPerJob walltime queue premium account binaryName \
	     <<<$(echo "Absolute" "Partial" 90  \
		       8 "00:30:00" "debug" 0 "m2113" "vasp")
    else
	read exemptDirCompare includeDirCompare maxJobsPerRun \
	     noNodesPerJob walltime queue premium account binaryName \
	     <<<$(echo  "$7" "$8" "$9" \
			"${10}" "${11}" "${12}" "${13}" "${14}" "${15}")
    fi
    
    echo  "$rootPath $minDepth $maxDepth"
    echo  "$includeDirList $exemptDirList $modulelist"
    echo  "$exemptDirCompare $includeDirCompare $maxJobsPerRun $noNodesPerJob $walltime $queue $premium $account $binaryName"
    
    ##PROCESSING
    if [ $NERSC_HOST == 'edison' ]; then
	noProcsPerNode=24 #Edison-24, Cori-32
    elif [ $NERSC_HOST == 'cori' ]; then
	noProcsPerNode=32 #Edison-24, Cori-32
    fi
    
    noProcsPerJob=$((noNodesPerJob*noProcsPerNode))
    
    count=1
    fileNo=1
    outFile="$rootPath/run$fileNo.job"
    write_vaspheader_slurm $queue $walltime $account $premium  \
			   $outFile modulelist[@]
    for i in $(find $rootPath -mindepth $minDepth \
		    -maxdepth $maxDepth -type d | sort); do	
	echo "Job No: $count"
	exemptFlag=0
	if [ "$exemptDirCompare" == "Absolute" ]; then
	    for exemptDir  in "${exemptDirList[@]}"; do
		if [ "$exemptDir" == "$i" ] ; then
		    echo "exemptDir: $exemptDir is in Dir: $i\n"
		    exemptFlag=1
		    break
		fi
	    done
	elif [ "$exemptDirCompare" == "Partial" ]; then
	    for exemptDir  in "${exemptDirList[@]}"; do
		if [ ! "${i/$exemptDir}" == "$i" ] ; then
		    echo "exemptDir: $exemptDir is in Dir: $i\n"
		    exemptFlag=1
		    break
		fi
	    done
	else
	    printf "unknown exemptDirCompare value"
	fi
	

	if [ $exemptFlag -eq  0 ]; then
	    if [ "$includeDirCompare" == "Absolute" ]; then
		for includeDir  in "${includeDirList[@]}"; do
		    if [ "$includeDir" == "$i" ] &&  \
			   [ -f "$i/INCAR" ]; then
			if [ "$count" -gt "$maxJobsPerRun" ]; then
			    fileNo=$((fileNo+1))
			    count=1
			    outFile="$rootPath/run$fileNo.job"
			    write_vaspheader_slurm $queue $walltime  \
						   $account $premium  \
						   $outFile modulelist[@]
			fi
			printf "cd $i \n" >>$outFile
			printf "srun -n $noProcsPerJob -N $noNodesPerJob $binaryName  >stdout &\n" >> $outFile;
			printf "sleep 3 \n" >> $outFile;
			count=$((count+1))
			break
		    fi
		done
	    elif [ "$includeDirCompare" == "Partial" ]; then
		for includeDir  in "${includeDirList[@]}"; do
		    if [ ! "${i/$includeDir}" == "$i" ] &&  \
			   [ -f "$i/INCAR" ]; then
			if [ "$count" -gt "$maxJobsPerRun" ]; then
			    fileNo=$((fileNo+1))
			    count=1
			    outFile="$rootPath/run$fileNo.job"
			    write_vaspheader_slurm $queue $walltime  \
						   $account $premium  \
						   $outFile modulelist[@]
			fi
			printf "cd $i \n" >>$outFile
			printf "srun -n $noProcsPerJob -N $noNodesPerJob $binaryName  >stdout &\n" >> $outFile;
			printf "sleep 3 \n" >> $outFile;
			count=$((count+1))
			break
		    fi
		done
	    else
		printf "unknown includeDirCompare value"
	    fi
	fi
    done
    
    for ((j=1;j<=fileNo;j++)); do
	jobFile="$rootPath/run$j.job"
	noJobs=$(grep "cd" $jobFile | wc -l)
	noNodes=$((noNodesPerJob*(noJobs)))
	sed -i "/#SBATCH -N/c\#SBATCH -N "$noNodes" " $jobFile
	sed -i "\$d" $jobFile
	sed -i -e "\$await" $jobFile
    done
}


write_vaspjob_pbs()
{
    echo "Entering Function: write_vaspheader_pbs"
    
    declare -a modulelist=("${!1}")
    declare -a rundirList=("${!2}")
    read queue walltime calcPerJob noNodes procsPerNode procsPerCalc \
	 account premium outFile system batchJobs \
	 <<<$(echo  "$3" "$4" "$5" "$6" "$7" "$8" "$9" "${10}" \
	      "${11}" "${12}" "${13}")
    echo  "$queue $walltime $noNodes $procsPerNode  $account $premium $outFile $system $batchJobs"
    echo  "${modulelist[@]}"
    
    printf "#!/bin/bash\n" > $outFile
    printf "#PBS -S /bin/bash\n"  >> $outFile
    printf "#PBS -m a\n" >> $outFile
    printf "#PBS -M kw1@ornl.gov\n" >> $outFile
    printf "#PBS -N sample_job\n" >> $outFile
    printf "#PBS -q $queue\n" >> $outFile
    printf "#PBS -l walltime=$walltime\n" >> $outFile
    printf "#PBS -l nodes=$noNodes:ppn=$procsPerNode\n" >> $outFile

    if [[ $account == "None" ]]; then
       printf "WARNING: No Account Information Provided\n"
    else	
       printf "#PBS -A $account\n" >> $outFile
    fi

    if [[ $system == "Cades" ]]; then
	if [[ $batchJobs == "True" ]]; then
	    printf "#PBS -l naccesspolicy=singlejob\n" >> $outFile
	    printf "\n\n"  >> $outFile
	    printf "##LOAD MODULES\n" >>$outFile
	    # SO-Link: http://unix.stackexchange.com/a/106085/77596
	    for module in "${modulelist[@]}"; do
		printf "module load $module\n" >>$outFile
	    done
	    printf "\n\n" >>$outFile	
	    printf "CORES_PER_RUN=$procsPerCalc\n" >> $outFile
	    printf "cd \$PBS_O_WORKDIR\n" >> $outFile
	    printf "cat \$PBS_NODEFILE >pbs_nodefile\n\n" >> $outFile
	    printf "VASP_MPI_PER_RUN_FLAGS=\"-n \$CORES_PER_RUN\"\n" >> $outFile
	    printf "VASP_PER_RUN_FLAGS=\' 2>&1 >vasp.out\'\n" >> $outFile      	
	    printf "MULTI_VASP_MPI_FLAGS=\"-x OMP_NUM_THREADS=1 --mca btl self,sm,openib --mca btl_openib_receive_queues S,9216,256,128,32:S,65536,256,128,32 --mca orte_base_help_aggregate 0 --nooversubscribe\"\n\n" >> $outFile
	    printf "declare -a vrundirs=(${rundirList[*]})" >> $outFile
	    printf "\n\n" >>$outFile

	    printf "vrun=1\n" >>$outFile
	    printf "for vrundir in \${vrundirs[@]}
	    do
		start=\$(expr 1 + \$(expr \$((\$vrun-1)) \* \$CORES_PER_RUN ))
		end=\$(expr \$vrun \* \$CORES_PER_RUN)
		echo \"\$vrun \$start \$end\"
		awk -v start=\$start -v end=\$end 'NR >= start && NR <= end'  pbs_nodefile > \${vrundir}/pbs_nodefile
		cd \$vrundir
		mpirun \$MULTI_VASP_MPI_FLAGS \$VASP_MPI_PER_RUN_FLAGS -hostfile pbs_nodefile \$VASP 2>&1 >vasp.out &
		cd ../
		vrun=\$(expr \$vrun + 1)
	    done 
            wait \n" >>$outFile
	else
	    printf "\n\n"  >> $outFile
	    printf "##LOAD MODULES\n" >>$outFile
	    # SO-Link: http://unix.stackexchange.com/a/106085/77596
	    for module in "${modulelist[@]}"; do
		printf "module load $module\n" >>$outFile
	    done
	    printf "cd \$PBS_O_WORKDIR \n"  >>$outFile
	    printf "mpirun -n 128 -v --report-bindings \${VASP_MPI_FLAGS} \$VASP >stdout\n" >>$outFile
	fi
    else
	printf "ERROR: Only works for ORNL-Cades-Condo"
    fi    
}


create_neb_poscars()
{
    startConfig=$1
    endConfig=$2
    noImages=$3
    outPath=$4
    cd $outPath
    perl /global/homes/r/rbala/CodesScripts/myCodes/bitbucketRepos/codesscripts/vaspScripts/vtstscripts-902/nebmake.pl $startConfig $endConfig $noImages
    cd -
}

modify_neb_incar()
{
    dirPath=$1
    cneb=$2
    if [ -z "$3" ]; then
	read smear brion <<<$(echo 0 1)
    else
	read smear brion <<<$(echo $3 $4)
    fi
    echo $cneb $smear $brion
    sed -i '/LCLIMB/{h;s/.*/LCLIMB='"$cneb"'/};${x;/^$/{s//LCLIMB='"$cneb"'/;H};x}' $dirPath/INCAR
    sed -i '/ISMEAR/{h;s/.*/ISMEAR='"$smear"'/};${x;/^$/{s//ISMEAR='"$smear"'/;H};x}' $dirPath/INCAR
    sed -i '/IBRION/{h;s/.*/IBRION='"$brion"'/};${x;/^$/{s//IBRION='"$brion"'/;H};x}' $dirPath/INCAR    
}

modify_parallel_incar()
{
    dirPath="$1"
    nsim="$2"
    ncore="$3"
    npar="$4"

    echo "NSIM is : $nsim"
    echo "NPAR is : $npar"
    echo "NCORE is: $ncore"

    if [ -z "$nsim" ]; then
	echo "NSIM is empty"
	sed -i "/NSIM/c\ " $dirPath/INCAR
    else
	echo "NSIM is NOT empty"
	sed -i '/NSIM/{h;s/.*/NSIM='"$nsim"'/};${x;/^$/{s//NSIM='"$nsim"'/;H};x}' $dirPath/INCAR
    fi
    
    if [ -z "$npar" ]; then
	echo "NPAR is empty"
	sed -i "/NPAR/c\ " $dirPath/INCAR
    else
	echo "NPAR is NOT empty"
	sed -i '/NPAR/{h;s/.*/NPAR='"$npar"'/};${x;/^$/{s//NPAR='"$npar"'/;H};x}' $dirPath/INCAR
    fi    
    if [ -z "$ncore" ]; then
	echo "NCORE is empty"
	sed -i "/NCORE/c\ " $dirPath/INCAR
    else
	echo "NCORE is NOT empty"
	sed -i '/NCORE/{h;s/.*/NCORE='"$ncore"'/};${x;/^$/{s//NCORE='"$ncore"'/;H};x}' $dirPath/INCAR
    fi
}

copy_vasp_input()
{
    inPath=$1
    outPath=$2
    if [ -z "$3" ]; then
	read copyExtraFiles <<<$(echo 0)
    else
	copyExtraFiles=$3
    fi
    cp $inPath/INCAR $outPath/INCAR
    cp $inPath/POTCAR $outPath/POTCAR
    cp $inPath/KPOINTS $outPath/KPOINTS
    if [ -f "$inPath/CONTCAR" ]; then
	cp $inPath/CONTCAR $outPath/POSCAR
    else
	cp $inPath/POSCAR $outPath/POSCAR
    fi
    if [ $copyExtraFiles -gt 0 ]; then
	if [ -f "$inPath/WAVECAR" ]; then
	    cp $inPath/WAVECAR $outPath/WAVECAR
	fi
	if [ -f "$inPath/CHGCAR" ]; then
	    cp $inPath/CHGCAR $outPath/CHGCAR
	fi
    fi
}

change_cubicposcar_vol()
{
    filePath=$1
    strainVal=$2
    strainType=$3
    if [ -z "$3" ]; then
	strainDir="All"
    else
	strainDir=$4
    fi
    
    read lineNoX lineNoY lineNoZ <<<$(echo 3 4 5)
    lengthX=$(sed -n ""$lineNoX"p" $filePath | \
		     awk -F " " '{print $1}')
    zeroVal=$(sed -n ""$lineNoX"p" $filePath | \
		     awk -F " " '{print $2}')
    deltaX=$(echo $lengthX*$strainVal | bc)
    if [ $strainType -eq 1 ]; then
	lengthXFin=$(echo $lengthX+$deltaX | bc)
    elif [ $strainType -eq -1 ]; then
	lengthXFin=$(echo $lengthX-$deltaX | bc)
    else
	echo "Unknown Parameter"
    fi
    if [ "$strainDir" == "All" ]; then	
	sed -i -e "3s/.*/    "$lengthXFin" "$zeroVal" "$zeroVal"/" $filePath
	sed -i -e "4s/.*/    "$zeroVal"  "$lengthXFin" "$zeroVal"/" $filePath
	sed -i -e "5s/.*/    "$zeroVal" "$zeroVal" "$lengthXFin"/" $filePath
    elif [ "$strainDir" == "Epitaxy" ]; then
	sed -i -e "3s/.*/    "$lengthXFin" "$zeroVal" "$zeroVal"  F  F  F/" $filePath
	sed -i -e "4s/.*/    "$zeroVal"  "$lengthXFin" "$zeroVal"  F  F  F/" $filePath
	sed -i -e "5s/.*/    "$zeroVal" "$zeroVal" "$lengthX"  F  F  T/" $filePath
    else
	echo "Unknown Strain Direction Parameter"
    fi
    
}

### CALCULATION RESTART BLOCK START ###

get_restart_count()
{
    # Validate if a variable is a number Link:
    # http://stackoverflow.com/a/806923/1652217
    read dirPath minDepth maxDepth <<<$(echo "$1" "$2" "$3")
    read fileName separator tarzip <<<$(echo "$4" "$5" "$6")
    re_int='^[0-9]+$'
    restartNo=0
    for i in $(find $dirPath -mindepth $minDepth -maxdepth $maxDepth \
		    -name "$fileName$separator*" -type f | sort -r); do
	if [ "$tarzip" == "False" ]; then
	    restartVal=$(echo $i| awk -F $separator '{print $NF}');
	elif [ "$tarzip" == "True" ]; then
	    restartVal=$(echo $i| awk -F [$separator.] '{print $(NF-1)}');
	else
	    echo "Unknown tarzip value"
	fi
	
	if ! [[ $restartVal =~ $re_int ]] ; then
	    echo "$i: Not a number"
	else
	    restartNo=$((restartVal+1))
	    break
	fi
    done
    if [ $restartNo -eq 0 ]; then
	restartNo=$((restartVal+1))
    fi	    
    echo $restartNo 	    
}

copy_vasp_files()
{
    read dirPath restartNo neb noImages tarzip  \
	 <<<$(echo "$1" "$2" "$3" "$4" "$5" )    
    fileName="vaspData_$restartNo.tgz"
    echo "$dirPath $restartNo $neb $noImages $tarzip"
    if [ "$neb" == "False" ]; then
	if [ "$tarzip" == "False" ]; then
	    mv $dirPath/POSCAR $dirPath/POSCAR_$restartNo
	    cp $dirPath/CONTCAR $dirPath/POSCAR
	    mv $dirPath/CONTCAR $dirPath/CONTCAR_$restartNo
	    mv $dirPath/XDATCAR $dirPath/XDATCAR_$restartNo
	    mv $dirPath/OUTCAR $dirPath/OUTCAR_$restartNo
	    mv $dirPath/OSZICAR $dirPath/OSZICAR_$restartNo	 
	elif [ "$tarzip" == "True" ]; then
	    tar -czf $dirPath/$fileName $dirPath/POSCAR $dirPath/CONTCAR \
		$dirPath/XDATCAR $dirPath/OUTCAR $dirPath/OSZICAR
	    yes | cp $dirPath/CONTCAR $dirPath/POSCAR
	else
	    echo "Unknown tarzip value"
	fi	
    elif [ "$neb" == "True" ]; then
	for i in $(seq -f "%02g" 1 $noImages); do
	    if [ "$tarzip" == "False" ]; then
		mv $dirPath/$i/POSCAR $dirPath/$i/POSCAR_$restartNo
		cp $dirPath/$i/CONTCAR $dirPath/$i/POSCAR
		mv $dirPath/$i/CONTCAR $dirPath/$i/CONTCAR_$restartNo
		mv $dirPath/$i/XDATCAR $dirPath/$i/XDATCAR_$restartNo
		mv $dirPath/$i/OUTCAR $dirPath/$i/OUTCAR_$restartNo
		mv $dirPath/$i/OSZICAR $dirPath/$i/OSZICAR_$restartNo	 
	    elif [ "$tarzip" == "True" ]; then		
		tar -czf $dirPath/$i/$fileName $dirPath/$i/POSCAR \
		    $dirPath/$i/CONTCAR $dirPath/$i/XDATCAR \
		    $dirPath/$i/OUTCAR $dirPath/$i/OSZICAR
		yes | cp $dirPath/$i/CONTCAR $dirPath/$i/POSCAR
	    else
		echo "Unknown tarzip value"
	    fi
	done
    else
	echo "Unknown neb variable value"
    fi
}



vasp_backup_for_restart()
{    
    read rootPath minDepth maxDepth <<<$(echo "$1" "$2" "$3")
    if [ -z "$4" ]; then
	includeDirList=("/")
	exemptDirList=("")
    else
	declare -a includeDirList=("${!4}")
	declare -a exemptDirList=("${!5}")
    fi
    
    if [ -z "$6" ]; then
	read exemptDirCompare includeDirCompare \
	     neb noImages tarzip <<<$(echo "Absolute" \
					   "Partial" "False" \
					   "0" "False")
    else
	read exemptDirCompare includeDirCompare \
	     neb noImages tarzip <<<$(echo "$6" "$7" "$8" "$9" "${10}")
    fi

    ## TEST CODE START
    echo "## VASP BACKUP INPUT VARIABLES START: ##"
    echo "$exemptDirCompare $includeDirCompare \
	     $neb $noImages $tarzip"
    echo "## VASP BACKUP INPUT VARIABLES END: ##"
    ## TEST CODE END
    
    for i in $(find $rootPath -mindepth $minDepth \
		    -maxdepth $maxDepth -type d | sort); do	
	exemptFlag=0
	if [ "$exemptDirCompare" == "Absolute" ]; then
	    for exemptDir  in "${exemptDirList[@]}"; do
		if [ "$exemptDir" == "$i" ] ; then
		    echo "exemptDir: $exemptDir is in Dir: $i\n"
		    exemptFlag=1
		    break
		fi
	    done
	elif [ "$exemptDirCompare" == "Partial" ]; then
	    for exemptDir  in "${exemptDirList[@]}"; do
		if [ ! "${i/$exemptDir}" == "$i" ] ; then
		    echo "exemptDir: $exemptDir is in Dir: $i\n"
		    exemptFlag=1
		    break
		fi
	    done
	else
	    printf "unknown exemptDirCompare value"
	fi
	
	if [ $exemptFlag -eq  0 ]; then
	    if [ "$includeDirCompare" == "Absolute" ]; then
		for includeDir  in "${includeDirList[@]}"; do
		    if [ "$includeDir}" == "$i" ] &&  \
			   [ -f "$i/INCAR" ]; then
			if [ "$neb" == "False" ]; then
			    locdirVal=$i
			elif [ "$neb" == "True" ]; then			    
			    locdirVal=$i/01
			else
			    echo "Unknown neb variable value"
			fi
			if [ "$tarzip" == "False" ]; then
			    fileName="POSCAR"
			elif [ "$tarzip" == "True" ]; then		
			    fileName="vaspData"
			else
			    echo "Unknown tarzip variable value"
			fi
			# restart_count_input=("$locdirVal" 1 1
			# 		     "$fileName" "_")
			# local restartNo=$(get_restart_count restart_count_input[@])
			local restartNo=$(get_restart_count $locdirVal 1 1 \
							    $fileName "_" \
							    $tarzip)
			echo "Folder path: $i"
			echo "Restart count: $restartNo"
			copy_vasp_files $i $restartNo $neb \
					$noImages $tarzip
			break
		    fi
		done
	    elif [ "$includeDirCompare" == "Partial" ]; then
		for includeDir  in "${includeDirList[@]}"; do
		    if [ ! "${i/$includeDir}" == "$i" ] &&  \
		    	   [ -f "$i/INCAR" ]; then
			if [ "$neb" == "False" ]; then
			    locdirVal=$i
			elif [ "$neb" == "True" ]; then			    
			    locdirVal=$i/01
			else
			    echo "Unknown neb variable value"
			fi
			if [ "$tarzip" == "False" ]; then
			    fileName="POSCAR"
			elif [ "$tarzip" == "True" ]; then		
			    fileName="vaspData"
			else
			    echo "Unknown tarzip variable value"
			fi
			local restartNo=$(get_restart_count $locdirVal 1 1 \
							    $fileName "_" \
							    $tarzip)
			echo "Folder path: $i"
			echo "Restart count: $restartNo"
			copy_vasp_files $i $restartNo $neb \
					$noImages $tarzip
			break
		    fi
		done
	    else
		printf "unknown includeDirCompare value"
	    fi
	fi
    done
    
}

### CALCULATION RESTART BLOCK END ###

return_path_with_matching_string()
{
    echo "TO BE IMPLEMENTED"
}


count_num_subdirs()
{
    rootPath=$1
    minDepth=$2
    maxDepth=$3
    exemptDirList=$4

    count=0
    for i in $(find $rootPath -mindepth $minDepth \
		    -maxdepth $maxDepth -type d | sort); do
	if [ ! -z $exemptDirList ]; then
	    flagExemptDir=0
	    for exemptDir in "${exemptDirList[@]}"; do
		if [ ! "${i/$exemptDir}" = "$i" ] ; then
		    printf "SKIPPING Dir $i containing exempt subdir: $exemptDir \n"
		    flagExemptDir=1
		    break
		fi
	    done
	    if [ $flagExemptDir -eq 0 ]; then
		count=$((count+1))
	    fi	
	else
	    count=$((count+1))		
	fi
    done
    printf "\n\n\n"
    printf "TOTAL NO. OF SUBDIRS IS: $count \n"
}

scratch_code_job()
{
    rootPath=$1
    minDepth=$2
    maxDepth=$3
    if [ -z "$4" ]; then
	includeDirList=("/")
	exemptDirList=("")
	modulelist=('vasp' 'python')
    else
	declare -a includeDirList=("${!4}")
	declare -a exemptDirList=("${!5}")
	declare -a modulelist=("${!6}")
    fi    
    if [ -z "$7" ]; then
	read maxJobsPerRun noNodesPerJob walltime queue premium account binaryName \
	     <<<$(echo 90 8 "00:30:00" "debug" 0 "m2113" "vasp")
    else
	read maxJobsPerRun noNodesPerJob walltime queue premium account binaryName \
	     <<<$(echo  "$7" "$8" "$9" "${10}" "${11}" "${12}" "${13}")
    fi
    
    echo  "$launchPath $minDepth $maxDepth"
    echo  "$includeDirList $exemptDirList $modulelist"
    echo  "$maxJobsPerRun $noNodesPerJob $walltime $queue $premium $account $binaryName"
    
    ##PROCESSING
    if [ $NERSC_HOST == 'edison' ]; then
	noProcsPerNode=24 #Edison-24, Cori-32
    elif [ $NERSC_HOST == 'cori' ]; then
	noProcsPerNode=32 #Edison-24, Cori-32
    fi
    
    noProcsPerJob=$((noNodesPerJob*noProcsPerNode))
    
    count=1
    fileNo=1
    outFile="$rootPath/run$fileNo.job"
    write_vaspheader_slurm $queue $walltime $account $premium  \
			   $outFile modulelist[@]
    for i in $(find $rootPath -mindepth $minDepth \
		    -maxdepth $maxDepth -type d | sort); do
	echo "Job No: $count"
	# subDir=$(echo $i|awk -F "/" '{print $2}');
	# echo "Entering Dir: $i \n"
	for includeDir  in "${includeDirList[@]}"; do
	    if [ ! "${i/$includeDir}" = "$i" ] ; then
		for exemptDir  in "${exemptDirList[@]}"; do
		    if [ "${i/$exemptDir}" = "$i" ] ; then			 
			if [ "$count" -le "$maxJobsPerRun" ] &&  \
			       [ -f "$i/POSCAR" ]; then
			    #printf "cd $PWD/$subDir \n" >> $outFile;
			    printf "cd $i \n" >>$outFile
			    printf "srun -n $noProcsPerJob -N $noNodesPerJob $binaryName  >stdout &\n" >> $outFile;
			    printf "sleep 3 \n" >> $outFile;
			    count=$((count+1))
			elif [ "$count" -gt "$maxJobsPerRun" ] &&  \
				 [ -f "$i/POSCAR" ]; then
			    fileNo=$((fileNo+1))
			    count=1
			    outFile="$rootPath/run$fileNo.job"
			    write_vaspheader_slurm $queue $walltime  \
						   $account $premium  \
						   $outFile modulelist[@]
			    printf "cd $i \n" >>$outFile
			    printf "srun -n $noProcsPerJob -N $noNodesPerJob $binaryName  >stdout &\n" >> $outFile;
			    printf "sleep 3 \n" >> $outFile;
			fi
		    else
			echo "exemptDir: $exemptDir is in Dir: $i\n"
			break [2]
		    fi
		done
	    else
		echo "includeDir: $includeDir is not in Dir: $i \n"
	    fi
	done
    done
    printf "wait" >>$outFile
    noNodes=$((noNodesPerJob*(count-1)))
    sed -i "/#SBATCH -N/c\#SBATCH -N "$noNodes" " $outFile
    echo "TOTAL NO. OF JOBS: $count \n"

}

# To enable run the codes inside functions directly from terminal
# bash createApRun.sh func_name arg1 arg2 ... argN
"$@"

