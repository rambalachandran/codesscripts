#/usr/local/bin/python


########## FIREWORKS CODE
"""
FIXME (Merge all the get_value functions by providing a second parameter to function)
FIXME (Merge all formation energy functions together)
REQ (CHECK IF THE FW_IDS have converged)
"""
import os
import sys
import math
from pymatgen.core.structure import Structure
import numpy as np
from pprint import pprint
import csv
import re
sys.path.append("/Users/kw1/Documents/myCodes/gitlab_ornl/PerovskitesIonTransport")
from analyzePerovskite import analyzePerovskite3D

# set up the LaunchPad and reset it


if __name__ == '__main__':
  
    rootPath = '/Users/kw1/Documents/ResearchProjects/IonicConductivity/Data/Perovskites/Analysis/BZO_Paper/DistortAnalysis/data'
    dirPathInit = 'K1_OH'
    fileNameInit = 'CONTCAR_BZO_K1_CHG_N1'
    dirPathFinal = dirPathInit
    fileNameFinal = 'CONTCAR_K1_OH_Oindex66_dir-X_CHG0'
    sysType = fileNameFinal.split('CONTCAR_', 1)[1]  # 'Doped-DopOH' or 'Cubic-DopedStruc'
    typeOfDisp = 'All'  # 'Dop-1NN' or 'All'
    dopantType = 'K'
    dopantAtomNo = 0
    analyzePerovskite = analyzePerovskite3D()
    filePathInit = os.path.join(rootPath, dirPathInit,
                                fileNameInit)
    filePathFinal = os.path.join(rootPath, dirPathFinal,
                                 fileNameFinal)
    # print filePathInit, filePathFinal  # TEST
    crystalStrucInit = Structure.from_file(filePathInit)
    crystalStrucFinal = Structure.from_file(filePathFinal)
    oxyDopantDistance = crystalStrucInit.lattice.a/6.0 + 1.0 # FIXME: Works only for 3x3x3 cubic cell.
    maxDisplacement = crystalStrucInit.lattice.a/(6*math.pow(2, 0.5)) # FIXME: Works only for a 3x3x3 cubic cell.
    dopantIndex = analyzePerovskite.return_index_of_atomtype(crystalStrucInit, [dopantType,])[dopantType][dopantAtomNo]
    if typeOfDisp == 'Dop-1NN':
        oxyNeighborsInit = [x for x in crystalStrucInit.get_neighbors(crystalStrucInit[dopantIndex], oxyDopantDistance, True) if x[0].species_string == 'O']
        oxyNeighborsInit_Index = [x[2] for x in oxyNeighborsInit]
    elif typeOfDisp == 'All':
        oxyNeighborsInit_Index = analyzePerovskite.return_index_of_atomtype(crystalStrucInit, ['O',])['O']
    distanceList = analyzePerovskite.calc_displacement(crystalStrucInit,
                                                       crystalStrucFinal,
                                                       maxDisplacement,
                                                       oxyNeighborsInit_Index,
                                                       frac_coord=True)

    arrDistanceVal = np.asarray([x[2] for x in distanceList])                 
    avgDisp = np.mean(arrDistanceVal)
    stdDisp = np.std(arrDistanceVal)
    maxDisp = max(arrDistanceVal)
    minDisp = min(arrDistanceVal)
    print 'Dopant-Type', dopantType
    print 'Dopant-Index: ', dopantIndex
    #print 'distanceList', pprint(distanceList)
    print 'Max.Displacement (in Ang)', maxDisp
    print 'Min.Dev Displacement (in Ang)', minDisp
    print 'Avg.Displacement (in Ang)', avgDisp
    print 'Std.Dev Displacement (in Ang)', stdDisp
        # displacement_list =[dopantType, maxDisp, minDisp, avgDisp, stdDisp]
    # headerVals = ['Dopant', 'Max. Displacement (Ang)', 'Min. Displacement (Ang)',
    #               'Avg. Displacement (Ang)', 'Std. Displacement (Ang)']
    outFileName = '_'.join(['disp-O', typeOfDisp, sysType, 'DopIndex',
                            str(dopantIndex), 'Struc.csv'])
    headerVals = ['IndexInit', 'IndexFinal', 'DistanceVal']
    with open(os.path.join(rootPath, dirPathFinal, outFileName), "w") as f:
        writer = csv.writer(f, delimiter=',')
        writer.writerow(headerVals)
        writer.writerows(distanceList)




    '''
    rootPath = '/scratch1/scratchdirs/rbala/Proj_m2113/BZO_Dopants_Adsorp_Paper_MRS'
    #### Displacement Cubic-DopedStruc ####
    dirPathInit = '/scratch1/scratchdirs/rbala/Proj_m2113/BZO_Dopants_Adsorp_Paper_MRS/Dopants_Relax/'
    fileNameInit = 'POSCAR'
    dirPathFinal = '/scratch1/scratchdirs/rbala/Proj_m2113/BZO_Dopants_Adsorp_Paper_MRS/Dopants_Relax_ChgCalc'
    fileNameFinal = 'CONTCAR'
   
    #### Displacement Cubic-DopedStruc ####

    dirPathInit = '/scratch1/scratchdirs/rbala/Proj_m2113/BZO_Dopants_Adsorp_Paper_MRS/Dopants_Intst_Relax_Full'
    fileNameInit = 'POSCAR'
    dirPathFinal = '/scratch1/scratchdirs/rbala/Proj_m2113/BZO_Dopants_Adsorp_Paper_MRS/Dopants_Intst_Relax_Full'
    fileNameFinal = 'CONTCAR'

for item in os.listdir(dirPathInit):
        if os.path.isdir(os.path.join(dirPathInit, item)):
            subDirList.append(item)
    displacement_list = []
    for item in subDirList:
        dopantType = re.split('_', item)[1]
    '''
      
        
    '''
    filePathInit = '/global/cscratch1/sd/rbala/Proj_m2113/Perovskites/BZO_Dopants_Adsorp_Paper_MRS/EdisonData/Dopants_Relax/M_Y/POSCAR'
    filePathFinal = '/global/cscratch1/sd/rbala/Proj_m2113/Perovskites/BZO_Dopants_Adsorp_Paper_MRS/EdisonData/Dopants_Relax_ChgCalc/M_Y/CONTCAR'
    dopantType = 'Y'
    
    analyzePerovskite = analyzePerovskite3D()
    crystalStrucInit = Structure.from_file(filePathInit)
    crystalStrucFinal = Structure.from_file(filePathFinal)
    oxyDopantDistance = crystalStrucInit.lattice.a/6.0 +1.00 # FIXME: Works only for 3x3x3 cubic cell.
    maxDisplacement = 1/(6*math.pow(2, 0.5)) # FIXME: Works only for Fractional Coordinates in a 3x3x3 cubic cell.
    dopantIndex = analyzePerovskite.return_index_of_atomtype(crystalStrucInit, list(dopantType))[dopantType]
    for index in dopantIndex:
        oxyNeighborsInit = [x for x in crystalStrucInit.get_neighbors(crystalStrucInit[index], oxyDopantDistance, True) if x[0].species_string == 'O']
        oxyNeighborsInit_Index = [x[2] for x in oxyNeighborsInit]
        print 'Main Code', oxyNeighborsInit_Index, maxDisplacement
        avgDistance, distanceList = analyzePerovskite.calc_displacement(crystalStrucInit,
                                                                        crystalStrucFinal,
                                                                        maxDisplacement,
                                                                        oxyNeighborsInit_Index,
                                                                        frac_coord=True,
                                                                        verbose=True)
                                                                        
       
        print 'Dopant Index' ,index
        print 'Oxygen Neighbors', pprint(oxyNeighborsInit)
        print 'Avg. Distance', avgDistance
        print 'DistanceList', pprint(distanceList)
    '''                                                         

