#/usr/local/bin/python


########## FIREWORKS CODE
"""
FIXME (Merge all the get_value functions by providing a second parameter to function)
FIXME (Merge all formation energy functions together)
REQ (CHECK IF THE FW_IDS have converged)
"""
import os,sys
import math
from fireworks import Firework, LaunchPad, ScriptTask, Workflow, Launch
from fireworks.core.rocket_launcher import launch_rocket
from fireworks_vasp.tasks import WriteVaspInputTask, VaspCustodianTask, VaspAnalyzeTask
from pymatgen.core.structure import Structure
from pymatgen.io.vaspio import Poscar
from pymatgen.core.periodic_table import Element
#import matplotlib.pyplot as plt
import numpy as np
from pprint import pprint
import csv
import gzip
import yaml
import datetime
import re
sys.path.append("/global/homes/r/rbala/CodesScripts/myCodes/bitbucketRepos/codesscripts/mrsCommScripts")
from analyzePerovskite import analyzePerovskite3D
from analyzePerovskite import pointDefects
from analyzePerovskite import Miscellaneous
from analyzePerovskite import inputOutput
# set up the LaunchPad and reset it

if __name__ == '__main__':
    list1 = ['La', 'Zr', 'Ba', 'Y', 'V']
    indexVal = Miscellaneous.find_recursive('Y', list1)
    print indexVal
    '''
    filePathInit = '/global/cscratch1/sd/rbala/Proj_m2113/Perovskites/BZO_Dopants_Adsorp_Paper_MRS/EdisonData/Dopants_Intst_Relax_Full/M_Ho/POSCAR'
    filePathFinal = '/global/cscratch1/sd/rbala/Proj_m2113/Perovskites/BZO_Dopants_Adsorp_Paper_MRS/EdisonData/Dopants_Intst_Relax_Full/M_Ho/CONTCAR'
    crystalStrucInit = Structure.from_file(filePathInit)
    crystalStrucFinal = Structure.from_file(filePathFinal)
    atomIndexInit = 78
    atomIndexFinal = 58
    frac_coord_Init = crystalStrucInit[atomIndex].frac_coords
    frac_coord_Final = crystalStrucFinal[atomIndex].frac_coords
    distance = crystalStrucFinal.lattice.get_distance_and_image(frac_coord_Init, frac_coord_Final)
    print frac_coord_Init, frac_coord_Final, distance
    '''
