#/usr/local/bin/python


########## FIREWORKS CODE
"""
FIXME (Merge all the get_value functions by providing a second parameter to function)
FIXME (Merge all formation energy functions together)
REQ (CHECK IF THE FW_IDS have converged)
"""
import os,sys
import math
from fireworks import Firework, LaunchPad, ScriptTask, Workflow, Launch
from fireworks.core.rocket_launcher import launch_rocket
from fireworks_vasp.tasks import WriteVaspInputTask, VaspCustodianTask, VaspAnalyzeTask
from pymatgen.core.structure import Structure
from pymatgen.io.vaspio import Poscar
from pymatgen.core.periodic_table import Element
#import matplotlib.pyplot as plt
import numpy as np
from pprint import pprint
import csv
import gzip
import yaml
import datetime
import re
sys.path.append("/global/homes/r/rbala/CodesScripts/myCodes/bitbucketRepos/codesscripts/mrsCommScripts")
from analyzePerovskite import analyzePerovskite3D

# set up the LaunchPad and reset it

if __name__ == '__main__':
    rootPath = '/scratch1/scratchdirs/rbala/Proj_m2113/BZO_Dopants_Adsorp_Paper_MRS'
    
    #### O - Displacement Cubic-DopedStruc ####
    '''
    dirPathInit = '/scratch1/scratchdirs/rbala/Proj_m2113/BZO_Dopants_Adsorp_Paper_MRS/Dopants_Relax/'
    fileNameInit = 'POSCAR'
    dirPathFinal = '/scratch1/scratchdirs/rbala/Proj_m2113/BZO_Dopants_Adsorp_Paper_MRS/Dopants_Relax_ChgCalc'
    fileNameFinal = 'CONTCAR'
    atomType = 'O'
    '''
    #### O - Displacement DopOH-DopedStruc ####
    '''
    dirPathInit = '/scratch1/scratchdirs/rbala/Proj_m2113/BZO_Dopants_Adsorp_Paper_MRS/Dopants_Intst_Relax_Full'
    fileNameInit = 'POSCAR'
    dirPathFinal = '/scratch1/scratchdirs/rbala/Proj_m2113/BZO_Dopants_Adsorp_Paper_MRS/Dopants_Intst_Relax_Full'
    fileNameFinal = 'CONTCAR'
    atomType = 'O'
    '''
    #### H - Displacement DopOH-DopedStruc ####
    dirPathInit = '/scratch1/scratchdirs/rbala/Proj_m2113/BZO_Dopants_Adsorp_Paper_MRS/Dopants_Intst_Relax_SD'
    fileNameInit = 'CONTCAR'
    dirPathFinal = '/scratch1/scratchdirs/rbala/Proj_m2113/BZO_Dopants_Adsorp_Paper_MRS/Dopants_Intst_Relax_Full'
    fileNameFinal = 'CONTCAR'
    atomType = 'H'


    sysType = 'Cubic-DopedStruc'  # 'Doped-DopOH' or 'Cubic-DopedStruc'
    typeOfDisp = 'Dop-1NN'  # 'Dop-1NN' or 'All'
    if atomType == 'O':
        constDistance = 1.0 
    elif atomType == 'H':
        constDistance = 3.0 # FIXME: Assumes presence of only one H interstitial
    
    outFileName = '-'.join(['disp', atomType, typeOfDisp, sysType, '-Struc.csv'])
    analyzePerovskite = analyzePerovskite3D()
    subDirList = []
    for item in os.listdir(dirPathInit):
        if os.path.isdir(os.path.join(dirPathInit, item)):
            subDirList.append(item)
    displacement_list = []
    
    for item in subDirList:
        dopantType = re.split('_',item)[1]
        print 'Dopant-Type', dopantType
        filePathInit = os.path.join(dirPathInit, item, fileNameInit)
        filePathFinal = os.path.join(dirPathFinal, item, fileNameFinal)
        crystalStrucInit = Structure.from_file(filePathInit)
        crystalStrucFinal = Structure.from_file(filePathFinal)
        atomDopantDistance = crystalStrucInit.lattice.a/6.0 + constDistance # FIXME: Works only for 3x3x3 cubic cell.
        maxDisplacement = crystalStrucInit.lattice.a/(6*math.pow(2, 0.5)) # FIXME: Works only for a 3x3x3 cubic cell.
        dopantIndex = analyzePerovskite.return_index_of_atomtype(crystalStrucInit, [dopantType,])[dopantType]
        print 'Total Dopant Index Dict', analyzePerovskite.return_index_of_atomtype(crystalStrucInit, [dopantType,])
        print 'Total Dopant Index List', dopantIndex
        for indexTmp in dopantIndex:
            #print 'Dopant Index', index
            index = dopantIndex[0]  # BUG: Hardcoded. Needs to be carefully considered to see why its not working
            tempList = []
            if typeOfDisp == 'Dop-1NN':
                atomNeighborsInit = [x for x in crystalStrucInit.get_neighbors(crystalStrucInit[index], atomDopantDistance, True) if x[0].species_string == atomType]
                atomNeighborsInit_Index = [x[2] for x in atomNeighborsInit]
            elif typeOfDisp == 'All':
                atomNeighborsInit_Index = analyzePerovskite.return_index_of_atomtype(crystalStrucInit, [atomType,])[atomType]
            distanceList = analyzePerovskite.calc_displacement(crystalStrucInit,
                                                               crystalStrucFinal,
                                                               maxDisplacement,
                                                               atomNeighborsInit_Index,
                                                               frac_coord=True)
            
            print 'distanceList', pprint(distanceList)
            
            arrDistanceVal = [x[2] for x in distanceList]
            atomNeighborsFinal_Index = [x[1] for x in distanceList]
            avgDisp = np.mean(arrDistanceVal)
            stdDisp = np.std(arrDistanceVal)
            maxDisp = max(arrDistanceVal)
            minDisp = min(arrDistanceVal)
            
            print 'Max.Displacement (in Ang)', maxDisp
            print 'Min.Dev Displacement (in Ang)', minDisp
            print 'Avg.Displacement (in Ang)', avgDisp
            print 'Std.Dev Displacement (in Ang)', stdDisp
            tempList = [dopantType, maxDisp, minDisp, avgDisp, stdDisp]
            if (atomType =='H'):
                dopantAtomDistanceInit = crystalStrucInit.get_distance(index,
                                                                       atomNeighborsInit_Index[0]) # FIXME: Assumes only one H interstitial
                dopantAtomDistanceFinal = crystalStrucFinal.get_distance(index,
                                                                         atomNeighborsFinal_Index[0]) # FIXME: Assumes only one H interstitial
                print atomNeighborsInit_Index, dopantAtomDistanceInit, dopantAtomDistanceInit
                tempList += [dopantAtomDistanceInit, dopantAtomDistanceFinal,
                             dopantAtomDistanceInit-dopantAtomDistanceFinal]
            displacement_list.append(tempList)
            print '\n\n'

    headerVals = ['Dopant', 'Max. Displacement (Ang)', 'Min. Displacement (Ang)',
                  'Avg. Displacement (Ang)', 'Std. Displacement (Ang)']
    if (atomType == 'H'):
        headerVals += ['Dop_Atom_Dist_SD (Ang)', 'Dop_Atom_Dist_FR (Ang)', 'Delta_Dop_Atom_Dist (Ang)']
    with open(os.path.join(rootPath, outFileName), "w") as f:
        writer = csv.writer(f, delimiter='\t')
        writer.writerow(headerVals)
        writer.writerows(displacement_list)
        
