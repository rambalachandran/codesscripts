#/usr/local/bin/python

import os,sys
import math
# from fireworks import Firework, LaunchPad, ScriptTask, Workflow, Launch
# from fireworks.core.rocket_launcher import launch_rocket
# from fireworks_vasp.tasks import WriteVaspInputTask, VaspCustodianTask, VaspAnalyzeTask
from pymatgen.core.structure import Structure
from pymatgen.io.vasp import Poscar
from pymatgen.core.periodic_table import Element
#import matplotlib.pyplot as plt
import numpy as np
from pprint import pprint
import csv
import gzip
import yaml
import datetime
import re
sys.path.append("/Users/kw1/Desktop/kw1/Documents/myCodes/gitlab_ornl/PerovskitesIonTransport/")
from analyzePerovskite import analyzePerovskite3D
from analyzePerovskite import pointDefects
from analyzePerovskite import Miscellaneous
from analyzePerovskite import inputOutput

def get_intst_formation_energy(energy_OHStruc,
                                   energy_PureStruc,
                                   efermi_PureStruc,
                                   chargeVal,
                                   chemPot_H=-4.880197265):
    formEnergy = ((energy_OHStruc +
                   chargeVal*efermi_PureStruc) -
                  (energy_PureStruc + chemPot_H))
    return formEnergy

def get_dopant_formation_energy(energy_DopedStruc,
                                chemPot_BSite,
                                energy_PureStruc,
                                efermi_PureStruc,
                                chargeVal,
                                chemPot_Dopant):
    # FIXME chemPot_BSite only works for 2/4 Perovskites
    # chemPot_BSite = 3*bsite_EnergyPerAtom - 2*chemPot_O
    formEnergy = ((energy_DopedStruc + chemPot_BSite +
                   chargeVal*efermi_PureStruc) -
                  (energy_PureStruc + chemPot_Dopant))
    return formEnergy

def get_dopant_intst_interaction_energy(energy_Dop_OHStruc,
                                        energy_PureStruc,
                                        energy_DopedStruc,
                                        energy_OHStruc,
                                        ):
    intrEnergy = ((energy_Dop_OHStruc + energy_PureStruc) -
                  (energy_DopedStruc + energy_OHStruc))
    return intrEnergy

if __name__ == '__main__':
    """
    ## BZO Pure Structure Data
    The maximum valence band is    3.74638100000000
    The minimum conduction band is    6.93935000000000
    The Fermi level is    5.34286550000000
    The bandgap is    3.19296900000000
    Ground State Energy, E0 (eV) = -0.11272738E+04
    ## OTHER Data
    chemPot_O = -4.4633148
    bsite_Energy (Zr) = -9.5046
    """
    
    rootPath = '/Users/kw1/Desktop/kw1/Dropbox (ORNL)/LDRD-Ionic-Conduct/Manuscripts/MRS_Communications_Apr/Data'
    dopChemPotFile = 'dopantEnergyFromMP.csv'
    strucDopEnergyFile = 'Dopants_Relax_ChgCalc/energyVal.csv'
    strucDopEnergySPFile = 'Dopants_Relax_ChgCalc_FromCubic_SP/energyVal.csv'
    strucDop_OH_SD_EnergyFile = 'Dopants_Intst_Relax_SD/energyVal.csv'
    strucDop_OH_FR_EnergyFile = 'Dopants_Intst_Relax_Full/energyVal.csv'
    outFileName = 'FormationIntrEnergy_6Oct16.csv'
    energy_PureStruc = -0.11272738E+04
    energy_OHStruc = -0.11355284E+04
    chemPot_O =  -4.4633148
    bsite_Energy =  -9.5046
    efermi_PureStruc = 3.746381
    dopCharge = -1
    hydroxylCharge = 1
    
    dopChemPotList = inputOutput.csv_to_list(os.path.join(rootPath, dopChemPotFile), ',', skipHeader=True)
    print dopChemPotList
    strucDopEnergyList = inputOutput.csv_to_list(os.path.join(rootPath, strucDopEnergyFile), ',', skipHeader=True)
    strucDopEnergySPList = inputOutput.csv_to_list(os.path.join(rootPath, strucDopEnergySPFile), ',', skipHeader=True)
    strucDop_OH_SD_EnergyList = inputOutput.csv_to_list(os.path.join(rootPath, strucDop_OH_SD_EnergyFile), ',', skipHeader=True)
    strucDop_OH_FR_EnergyList = inputOutput.csv_to_list(os.path.join(rootPath, strucDop_OH_FR_EnergyFile), ',', skipHeader=True)
    chemPot_BSite = 3*bsite_Energy - 2*chemPot_O
    dopantList = [x[0] for x in dopChemPotList]
    analyzePerovskite = analyzePerovskite3D()
    MiscellaneousObj = Miscellaneous()
    #pointDefects = pointDefects()
    intstFormEnergy = get_intst_formation_energy(energy_OHStruc,
                                                 energy_PureStruc,
                                                 efermi_PureStruc,
                                                 hydroxylCharge)
    print 'Intst Formation Energy is', intstFormEnergy
    formIntrEnergyList = []
    for dopantType in dopantList:
        #print dopantType
        indexDop =  MiscellaneousObj.find_recursive(dopantType,
                                                    dopChemPotList)
        #print indexDop, indexDop[0], dopChemPotList[indexDop[0]][:]
        
        dopChemPot = 0.5*(dopChemPotList[indexDop[0]][2]*5 - 3*chemPot_O)
        
        indexDop = MiscellaneousObj.find_recursive(dopantType,
                                                   strucDopEnergyList)
        strucDopEnergy = strucDopEnergyList[indexDop[0]][1]

        indexDop = MiscellaneousObj.find_recursive(dopantType,
                                                   strucDopEnergySPList)
        strucDopEnergySP = strucDopEnergySPList[indexDop[0]][1]
        
        indexDop = MiscellaneousObj.find_recursive(dopantType,
                                                   strucDop_OH_SD_EnergyList)
        strucDop_OH_SD_Energy = strucDop_OH_SD_EnergyList[indexDop[0]][1]

        indexDop = MiscellaneousObj.find_recursive(dopantType,
                                                   strucDop_OH_FR_EnergyList)
        strucDop_OH_FR_Energy = strucDop_OH_FR_EnergyList[indexDop[0]][1]

        print (dopantType, dopChemPot, strucDopEnergySP,
               strucDopEnergy,  strucDop_OH_SD_Energy, strucDop_OH_FR_Energy)
        
        dopantFormEnergy = get_dopant_formation_energy(strucDopEnergy,
                                                       chemPot_BSite,
                                                       energy_PureStruc,
                                                       efermi_PureStruc,
                                                       dopCharge,
                                                       dopChemPot)
        dopantFormEnergySP = get_dopant_formation_energy(strucDopEnergySP,
                                                         chemPot_BSite,
                                                         energy_PureStruc,
                                                         efermi_PureStruc,
                                                         dopCharge,
                                                         dopChemPot)
        dopOHIntrEnergy_SD = get_dopant_intst_interaction_energy(strucDop_OH_SD_Energy,
                                                                 energy_PureStruc,
                                                                 strucDopEnergy,
                                                                 energy_OHStruc)
        
        dopOHIntrEnergy_FR = get_dopant_intst_interaction_energy(strucDop_OH_FR_Energy,
                                                                 energy_PureStruc,
                                                                 strucDopEnergy,
                                                                 energy_OHStruc)

        formIntrEnergyList.append([dopantType, dopantFormEnergySP, dopantFormEnergy,
                                   dopOHIntrEnergy_SD, dopOHIntrEnergy_FR])


    headerVals = ['Dopant', 'DopFormEnergy-SP(eV)', 'DopFormEnergy(eV)',
                  'DopOH-Intr-Energy-SD (eV)', 'DopOH-Intr-Energy-FR (eV)']
    with open(os.path.join(rootPath, outFileName), "w") as f:
        writer = csv.writer(f, delimiter=',')
        writer.writerow(headerVals)
        writer.writerows(formIntrEnergyList)
