#!/bin/bash

## Importing Other Scripts
source ./bashFunctionsForDFT.sh

## STEP-1
step1()
{
    inFolder=Dopants_Intst_Relax
    outFolder=Dopants_Relax
    for i in $(find $inFolder -mindepth 1 -maxdepth 1 -type d);
    do
	j=$(echo $i|awk -F "/" '{print $2}');
	dopType=$(echo $j| awk -F "_" '{print $2}');
	mkdir -p $outFolder/$j;
	cp $inFolder/$j/102/INCAR $outFolder/$j/INCAR ;
	cp $inFolder/$j/102/POSCAR $outFolder/$j/POSCAR;
	cp $inFolder/$j/102/POTCAR $outFolder/$j/POTCAR;
	cp $inFolder/$j/102/KPOINTS $outFolder/$j/KPOINTS;
	sed -i "/Ba Zr/c\Ba Zr "$dopType" O" $outFolder/$j/POSCAR;q
	sed -i '/27 26 1/c\27 26 1 81' $outFolder/$j/POSCAR;
	sed -i '$d' $outFolder/$j/POSCAR;
	sed -i.bkp -e '/PAW_PBE H 15Jun2001/,$d' $outFolder/$j/POTCAR;
    done
}

## STEP-2
step2()
{
    inFolder=Dopants_Relax
    outFolder=Dopants_Relax_ChgCalc
    for i in $(find $inFolder -mindepth 1 -maxdepth 1 -type d);
    do
	j=$(echo $i|awk -F "/" '{print $2}');
	dopType=$(echo $j| awk -F "_" '{print $2}');
	mkdir -p $outFolder/$j;
	cp $inFolder/$j/INCAR $outFolder/$j/INCAR ;
	cp $inFolder/$j/CONTCAR $outFolder/$j/POSCAR;
	cp $inFolder/$j/POTCAR $outFolder/$j/POTCAR;
	cp $inFolder/$j/KPOINTS $outFolder/$j/KPOINTS;
	elNo=$(grep NELECT $inFolder/$j/OUTCAR | awk -F " " '{print $3}'sed -n '111p' Dopants_Intst_Relax/M_Al/102/CONTCAR | awk -F " " '{print $3}');
	elNo=$(echo $elNo +1 | bc);
	sed -i -e "$ a NELECT  = "$elNo" " $outFolder/$j/INCAR;
    done

    
}

## STEP-3
step3()
{
    for i in $(find . -mindepth 1 -maxdepth 1 -type d);
    do
	cp run.job $i/run.job;
	cd $i;
	pwd;
	sed -i "/--time/c\#SBATCH --time=04:00:00" run.job;
	sbatch run.job;
	cd -;
    done
}

## STEP-4
step4()
{
    inFolder=Dopants_Relax_ChgCalc
    inFolderPP=Dopants_Intst_Relax
    outFolder=Dopants_Intst_Relax_SD
    deltaX=0.07623966892923767
    lineNo=111 #atomIndex 102
    hydLineNo=145
    sdLineNo=8
    for i in $(find $inFolder -mindepth 1 -maxdepth 1 -type d);
    do
	j=$(echo $i|awk -F "/" '{print $2}');
	dopType=$(echo $j| awk -F "_" '{print $2}');
	mkdir -p $outFolder/$j;
	cp $inFolder/$j/CONTCAR $outFolder/$j/POSCAR;
	cp $inFolder/$j/KPOINTS $outFolder/$j/KPOINTS;
	cp $inFolderPP/$j/102/INCAR $outFolder/$j/INCAR
	cp $inFolderPP/$j/102/POTCAR $outFolder/$j/POTCAR;
	#calculate H coordinates
	xVal=$(sed -n ""$lineNo"p" $inFolder/$j/CONTCAR | awk -F " " '{print $1}');
	yVal=$(sed -n ""$lineNo"p" $inFolder/$j/CONTCAR | awk -F " " '{print $2}');
	zVal=$(sed -n ""$lineNo"p" $inFolder/$j/CONTCAR | awk -F " " '{print $3}');
	xVal=$(echo $xVal+$deltaX | bc);
	# edit POSCAR
	sed -i "/ Ba /c\Ba Zr "$dopType" O H " $outFolder/$j/POSCAR;
	sed -i '/ 27 /c\27 26 1 81 1' $outFolder/$j/POSCAR;
	sed -i ""$sdLineNo"i Selective Dynamics" $outFolder/$j/POSCAR;
	sed -i -e ""$hydLineNo"i "$xVal" "$yVal" "$zVal" " $outFolder/$j/POSCAR;
	sed -i "10,144s/$/ F  F  F/" $outFolder/$j/POSCAR;
	sed -i "145s/$/ T  T  T/" $outFolder/$j/POSCAR;
    done
}

## STEP-5
step5()
{
    inFolder=Dopants_Relax_ChgCalc
    inFolderPP=Dopants_Intst_Relax
    outFolder=Dopants_Intst_Relax_Full
    deltaX=0.07623966892923767
    lineNo=111 #atomIndex 102
    hydLineNo=144
    #sdLineNo=8
    for i in $(find $inFolder -mindepth 1 -maxdepth 1 -type d);
    do
	j=$(echo $i|awk -F "/" '{print $2}');
	dopType=$(echo $j| awk -F "_" '{print $2}');
	mkdir -p $outFolder/$j;
	cp $inFolder/$j/CONTCAR $outFolder/$j/POSCAR;
	cp $inFolder/$j/KPOINTS $outFolder/$j/KPOINTS;
	cp $inFolderPP/$j/102/INCAR $outFolder/$j/INCAR
	cp $inFolderPP/$j/102/POTCAR $outFolder/$j/POTCAR;
	#calculate H coordinates
	xVal=$(sed -n ""$lineNo"p" $inFolder/$j/CONTCAR | awk -F " " '{print $1}');
	yVal=$(sed -n ""$lineNo"p" $inFolder/$j/CONTCAR | awk -F " " '{print $2}');
	zVal=$(sed -n ""$lineNo"p" $inFolder/$j/CONTCAR | awk -F " " '{print $3}');
	xVal=$(echo $xVal+$deltaX | bc);
	# edit POSCAR
	sed -i "/ Ba /c\Ba Zr "$dopType" O H " $outFolder/$j/POSCAR;
	sed -i '/ 27 /c\27 26 1 81 1' $outFolder/$j/POSCAR;
	#sed -i ""$sdLineNo"i Selective Dynamics" $outFolder/$j/POSCAR;
	sed -i -e ""$hydLineNo"i "$xVal" "$yVal" "$zVal" " $outFolder/$j/POSCAR;
	#sed -i "10,144s/$/ F  F  F/" $outFolder/$j/POSCAR;
	#sed -i "145s/$/ T  T  T/" $outFolder/$j/POSCAR;
    done
}

## STEP-6 Modify POSCAR to make it readable with Pymatgen for Full Relaxation
step6()
{
    inFolder=Dopants_Intst_Relax_Full_Test
    lineNo=144
    for i in $(find $inFolder -mindepth 1 -maxdepth 1 -type d);
    do
	sed -i -e "$ a 0.00000000E+00  0.00000000E+00  0.00000000E+00" $i/POSCAR;
    done
}

## STEP-7 Use perfect POSCAR and perform charged calculation just SP
step7()
{
    inFolder=Dopants_Relax_ChgCalc;
    inFolderPoscar=Dopants_Relax;
    outFolder=Dopants_Relax_ChgCalc_FromCubic_SP;
    for i in $(find $inFolder -mindepth 1 -maxdepth 1 -type d);
    do
	j=$(echo $i|awk -F "/" '{print $2}');
	dopType=$(echo $j| awk -F "_" '{print $2}');
	mkdir -p $outFolder/$j;
	cp $inFolder/$j/INCAR $outFolder/$j/INCAR;
	cp $inFolder/$j/KPOINTS $outFolder/$j/KPOINTS;
	cp $inFolder/$j/POTCAR $outFolder/$j/POTCAR;
	cp $inFolderPoscar/$j/POSCAR $outFolder/$j/POSCAR;
	sed -i '/IBRION/c\IBRION = -1' $outFolder/$j/INCAR;
	sed -i '/NSW/c\  ' $outFolder/$j/INCAR;
    done
}

## STEP-8 Input: Doped cubic POSCAR (i)add another dopant (ii) add vacancy and relax the whole structure
# Dopant atom.No: 54 [lineNo: 62]
# Other Bsite atom.No: 43 [lineNo: 51]
# vacancy atom.No: 70 [lineNo: 78]
step8()
{
    inFolder=Dopants_Relax;
    outFolder=Dop_Vac_Dop_Relax;
    for i in $(find $inFolder -mindepth 1 -maxdepth 1 -type d);
    do
	j=$(echo $i|awk -F "/" '{print $2}');
	dopType=$(echo $j| awk -F "_" '{print $2}');
	mkdir -p $outFolder/$j;
	cp $inFolder/$j/INCAR $outFolder/$j/INCAR;
	cp $inFolder/$j/KPOINTS $outFolder/$j/KPOINTS;
	cp $inFolder/$j/POSCAR $outFolder/$j/POSCAR;
	cp $inFolder/$j/POTCAR $outFolder/$j/POTCAR;
	sed -i "/Ba Zr/c\Ba Zr "$dopType" O " $outFolder/$j/POSCAR;
	sed -i '/27 2/c\27 25 2 80 ' $outFolder/$j/POSCAR;
	sed -i.bkp '51{h;d}; 62G' $outFolder/$j/POSCAR;
	sed -i.bkp2 '78d' $outFolder/$j/POSCAR;
    done
}

## STEP-9 Input: Doped and relaxed CONTCAR (noChg) and
# (i) add H atom along x axis in first case (on 2NN oxygen atom.No: 97 [lineNo: 105]
# (i) add H atom along z axis in second case (on 2NN oxygen atom.No: 97 [lineNo: 105]
# CLONE OF STEP-5
step9()
{
    inFolder=Dopants_Relax_ChgCalc;
    inFolderPP=Dopants_Intst_Relax_Full;
    #outFolder=Dopants_Intst_O2NN_dirX_Relax_Full;
    #deltaX=0.07623966892923767;
    outFolder=Dopants_Intst_O2NN_dirZ_Relax_Full;
    deltaZ=0.07623966892923767;
    lineNo=105 #atomIndex 96 (atomNo: 97);
    hydLineNo=144;
    #sdLineNo=8
    for i in $(find $inFolder -mindepth 1 -maxdepth 1 -type d);
    do
	j=$(echo $i|awk -F "/" '{print $2}');
	dopType=$(echo $j| awk -F "_" '{print $2}');
	mkdir -p $outFolder/$j;
	cp $inFolder/$j/CONTCAR $outFolder/$j/POSCAR;
	cp $inFolder/$j/KPOINTS $outFolder/$j/KPOINTS;
	cp $inFolderPP/$j/INCAR $outFolder/$j/INCAR
	cp $inFolderPP/$j/POTCAR $outFolder/$j/POTCAR;
	#calculate H coordinates
	xVal=$(sed -n ""$lineNo"p" $inFolder/$j/CONTCAR | awk -F " " '{print $1}');
	yVal=$(sed -n ""$lineNo"p" $inFolder/$j/CONTCAR | awk -F " " '{print $2}');
	zVal=$(sed -n ""$lineNo"p" $inFolder/$j/CONTCAR | awk -F " " '{print $3}');
	#xVal=$(echo $xVal+$deltaX | bc);
	zVal=$(echo $zVal+$deltaZ | bc);
	# edit POSCAR
	sed -i "/ Ba /c\Ba Zr "$dopType" O H " $outFolder/$j/POSCAR;
	sed -i '/ 27 /c\27 26 1 81 1' $outFolder/$j/POSCAR;
	#sed -i ""$sdLineNo"i Selective Dynamics" $outFolder/$j/POSCAR;
	sed -i -e ""$hydLineNo"i "$xVal" "$yVal" "$zVal" " $outFolder/$j/POSCAR;
	#sed -i "10,144s/$/ F  F  F/" $outFolder/$j/POSCAR;
	#sed -i "145s/$/ T  T  T/" $outFolder/$j/POSCAR;
    done
}

# STEP-10 Create input structure and folders to perform NEB calculation - GammaPoint
step10()
{
    inFolderStart=/scratch1/scratchdirs/rbala/Proj_m2113/BZO_Dopants_Adsorp_Paper_MRS/Dopants_Intst_O2NN_dirX_Relax_Full
    inFolderEnd=/scratch1/scratchdirs/rbala/Proj_m2113/BZO_Dopants_Adsorp_Paper_MRS/Dopants_Intst_O2NN_dirZ_Relax_Full
    outFolder=/scratch1/scratchdirs/rbala/Proj_m2113/BZO_Dopants_Adsorp_Paper_MRS/Dopants_Intst_O2NN_Neb_Rotate_XZ
    incarNebPath=/global/homes/r/rbala/softwares/vaspFiles/incarFiles/IBRION_1_ISMEAR_0_NEB
    kpointsPath=/global/homes/r/rbala/softwares/vaspFiles/kpointsFiles/Gamma_Auto_1x1x1
    noImages=5
    cneb="FALSE"
    smear=0
    brion=1
    nsim="4"
    ncore="24"
    npar="24"
    
    if [ ! -d "$outFolder" ]; then
	mkdir -p $outFolder
    fi   
    for i in $(find $inFolderStart -mindepth 1 -maxdepth 1 -type d); do
	subDir=$(echo $i|awk -F "/" '{print $NF}')
	echo $i $subDir
	startConfig=$inFolderStart/$subDir/CONTCAR
	endConfig=$inFolderEnd/$subDir/CONTCAR	
	outPath=$outFolder/$subDir/Kpt_1x1x1
	if [ ! -d "$outPath" ]; then
	    mkdir -p $outPath
	fi
	cp $incarNebPath $outPath/INCAR
	cp $kpointsPath $outPath/KPOINTS
	cp $inFolderStart/$subDir/POTCAR $outPath/POTCAR
	create_neb_poscars $startConfig $endConfig $noImages $outPath
	modify_neb_incar "$outPath" "$cneb" "$smear" "$brion"
	modify_parallel_incar "$outPath" "$nsim"  "$ncore" "$npar"
    done
}

# STEP-11 Create jobscript for neb calculation with gammapoint
step11()
{
    rootPath="/scratch1/scratchdirs/rbala/Proj_m2113/BZO_Dopants_Adsorp_Paper_MRS/Dopants_Intst_O2NN_Neb_Rotate_XZ"
    read minDepth maxDepth <<<$(echo 1 4)
    includeDirList="/"
    exemptDirList=("noStrain" "old" "miscTempTestOldOther")
    modulelist=("vasp/5.3.5_vtst" "python")
    read  maxJobsPerRun noNodesPerJob walltime queue premium account binaryName  \
	  <<<$(echo 90 25 "02:30:00" "regular" 0 "m2113" "vasp_gam")    
    write_vaspjob_slurm $rootPath $minDepth $maxDepth  \
			includeDirList[@] exemptDirList[@]  modulelist[@]  \
			$maxJobsPerRun $noNodesPerJob $walltime $queue  \
			$premium $account $binaryName
}



# STEP-12 backup data in VASP to restart calculation and create jobscript
step12()
{
    rootPath="/scratch1/scratchdirs/rbala/Proj_m2113/BZO_Dopants_Adsorp_Paper_MRS/Dopants_Intst_O2NN_Neb_Rotate_XZ"
    read minDepth maxDepth <<<$(echo 1 4)
    includeDirList="/"
    exemptDirList=("noStrain" "old" "miscTempTestOldOther")
    read  exemptDirCompare includeDirCompare neb noImages \
	  tarzip <<<$(echo  "Absolute" "Partial" "True" "05" "False")
    vasp_backup_for_restart $rootPath $minDepth $maxDepth  \
    			    includeDirList[@] exemptDirList[@] \
    			    $exemptDirCompare $includeDirCompare \
			    $neb $noImages $tarzip
    modulelist=("vasp/5.3.5_vtst" "python")
    read  maxJobsPerRun noNodesPerJob walltime queue premium account binaryName  \
	  <<<$(echo 90 25 "02:30:00" "regular" 0 "m2113" "vasp_gam")    
    write_vaspjob_slurm $rootPath $minDepth $maxDepth  \
			includeDirList[@] exemptDirList[@]  modulelist[@]  \
			$exemptDirCompare $includeDirCompare \
			$maxJobsPerRun $noNodesPerJob $walltime $queue  \
			$premium $account $binaryName    
}

# STEP-12b copy data from Kpt_1x1x1 to Kpt_2x2x2 and restart the calculation
step12b()
{
    rootPath="/scratch1/scratchdirs/rbala/Proj_m2113/BZO_Dopants_Adsorp_Paper_MRS/Dopants_Intst_O2NN_Neb_Rotate_XZ"
    kpointsPath="/global/homes/r/rbala/softwares/vaspFiles/kpointsFiles/Gamma_Auto_2x2x2"
    noImages=5
    
    #SUBSTEP-1
    minDir=0
    maxDir=$((noImages+1))
    echo "$maxDir"
    for i in $(find $rootPath -mindepth 1 -maxdepth 1 -type d | sort); do
    	mkdir $i/Kpt_2x2x2
    	cp $i/Kpt_1x1x1/INCAR $i/Kpt_2x2x2/INCAR
    	cp $i/Kpt_1x1x1/POTCAR $i/Kpt_2x2x2/POTCAR
    	cp $kpointsPath $i/Kpt_2x2x2/KPOINTS
    	for j in $(seq -f "%02g" $minDir $maxDir); do
    	    mkdir $i/Kpt_2x2x2/$j
    	    if [ -f $i/Kpt_1x1x1/$j/CONTCAR ]; then
    		cp $i/Kpt_1x1x1/$j/CONTCAR $i/Kpt_2x2x2/$j/POSCAR
    	    else
		echo "CONTCAR NOT PRESENT in $i/Kpt_1x1x1/$j"
    		cp $i/Kpt_1x1x1/$j/POSCAR $i/Kpt_2x2x2/$j/POSCAR
    	    fi
    	done
    done

    #SUBSTEP2
    printf "##### VASP JOBSCRIPT BEGIN #####\n\n"
    read minDepth maxDepth includeDirCompare  \
    	 exemptDirCompare <<<$(echo 1 4 "Partial" "Partial")
    includeDirList=("/")
    exemptDirList=("Kpt_1x1x1")
    modulelist=("vasp/5.3.5_vtst" "python")
    read  maxJobsPerRun noNodesPerJob walltime queue premium account binaryName  \
    	  <<<$(echo 90 25 "03:00:00" "regular" 0 "m2113" "vasp_std")
    write_vaspjob_slurm $rootPath $minDepth $maxDepth  \
    			includeDirList[@] exemptDirList[@]  modulelist[@]  \
    			$exemptDirCompare $includeDirCompare $maxJobsPerRun \
    			$noNodesPerJob $walltime $queue  \
    			$premium $account $binaryName
    printf "##### VASP JOBSCRIPT END #####\n\n"

}


##SCRATCH
scratch_code_dopants()
{
    echo "Hello World"
    # sed -n ""$lineNo"p" Dopants_Intst_Relax/M_Al/102/CONTCAR | awk -F " " '{print $3}'

    #  hydLineNo=145;
    #  sdLineNo=8
    #  sed -i ""$sdLineNo"i Selective Dynamics" CONTCAR;
    #  sed -i "10,144s/$/ F  F  F/" CONTCAR;
    #  sed -i "145s/$/ T  T  T/" CONTCAR;
    # sed -i "6,7s/^/   /" $i/POSCAR;
    # sed -i  ""$lineNo"s/^/  0/" $i/POSCAR;

}

# To enable run the codes inside functions directly from terminal
# bash createApRun.sh func_name arg1 arg2 ... argN
"$@"
