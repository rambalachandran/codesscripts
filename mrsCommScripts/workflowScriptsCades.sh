#!/bin/bash

## Importing Other Scripts
source ./bashFunctionsForDFT.sh


## SCRIPT TO ADD folders to wraprun in OLCF (TITAN)
##INPUT
modify_incar_parallel()
{
    inFolderPath=$1
    for i in $(find $inFolderPath -maxdepth 1 -type d \
		    -name "launcher_2016*"); do
	echo $i;
	sed -i '/NPAR/c\NPAR = 16' $i/INCAR
	sed -i '/NSIM/c\NSIM = 4' $i/INCAR
	sed -i '18i LPLANE = False' $i/INCAR
	sed -i '19i KPAR = 4' $i/INCAR	
    done
}

copy_output_files()
{
    inFolderPath=$1    
    for i in $(find $inFolderPath -maxdepth 1 -type d \
		    -name "launcher_2016*"); do

	if [ -f $i/2875668*.out ]; then
	    yes| cp $i/2875668*.out $i/vasp.out;
	elif [ -f $i/2875669*.out ];  then
	    yes| cp $i/2875669*.out $i/vasp.out;
	elif [ -f $i/2876522*.out ]; then
	    yes| cp $i/2876522*.out $i/vasp.out;
	elif [ -f $i/2876525*.out ]; then
	    yes| cp $i/2876525*.out $i/vasp.out;
	else
	    echo $i fi done
	fi
    done
}

create_vasp_gamma_run()
{
    read inFolderPath calcPerJob procsPerCalc walltime system \
	 <<<$(echo "$1" "$2" "$3" "$4" "$5")

    echo "PRINT: " $inFolderPath $calcPerJob $procsPerCalc $walltime $system
    declare -a incarVarList=('NPAR' 'NSIM' 'LPLANE' 'KPAR' \
				    'ICHARG' 'ISTART' 'ALGO' \
				    'PREC' 'EDIFFG')
    if [[ $system == "Cades" ]]; then
	read queue premium account procsPerNode \
	     <<<$(echo "batch" "0" "None" "32")	
    else
	printf "ERROR: Implemented only for Cades"
    fi
    if [ $calcPerJob -gt 1 ]; then
	batchJobs="True"
    else
	batchJobs="False"
    fi

    noNodes=$((calcPerJob*procsPerCalc/procsPerNode))
    # read queue walltime  premium account system batchJobs \
    # 	 <<<$(echo "batch" "24:00:00" "0" "None" "Cades" "True")
    
    modulelist=('env/cades-cnms' 'vasp')
    rundirList=()

    echo "InFolderPath: $inFolderPath"
    jobCount=0
    runFileCount=0   
    for i in $(find $inFolderPath -maxdepth 1 -type d \
		    -name "launcher_2016*"); do
	jobCount=$(($jobCount + 1))
	printf "$i\n\n"
		
	# MODIFY INCAR
	for incarVar in "${incarVarList[@]}"; do
	    sed -i "/$incarVar/c\ " $i/INCAR
	done
	if grep -q "IBRION" $i/INCAR ;  then
	    sed -i "/IBRION/c\IBRION = 2" $i/INCAR
	else
	    sed -i -e "\$a IBRION = 2" $i/INCAR
	fi
	if grep -q "POTIM" $i/INCAR ; then
	    sed -i '/POTIM/c\POTIM = 0.5' $i/INCAR
	else
	    sed -i -e "\$a POTIM = 0.5" $i/INCAR
	fi
	# if  grep -q "EDIFFG" $i/INCAR ; then
	#     sed -i '/EDIFFG/c\EDIFFG = -0.01' $i/INCAR
	# else
	#     sed -i '/EDIFF/a EDIFFG = -0.01' $i/INCAR

	# fi
	if  grep -q "NCORE" $i/INCAR ; then
	    sed -i '/NCORE/c\NCORE = 32' $i/INCAR
	else
	    sed -i -e "\$a NCORE = 32" $i/INCAR
	fi
	
	# MODIFY KPOINTS
	sed -i '/2 2 2/c\1 1 1' $i/KPOINTS

	# COPY CONTCAR to POSCAR
	cp $i/CONTCAR $i/POSCAR
	
	# JOB BATCHING
	rundirList+=("$i")
	if [ "$jobCount" -ge "$calcPerJob" ]; then
	    jobCount=0
	    outFile="$inFolderPath/run_cdco_vasp_$runFileCount.job"
	    printf "$batchJobs\n"
	    write_vaspjob_pbs  modulelist[@] rundirList[@] $queue \
				  $walltime $calcPerJob $noNodes \
				  $procsPerNode \
				  $procsPerCalc $account $premium \
				  $outFile  $system $batchJobs
	    rundirList=()
	    runFileCount=$(($runFileCount + 1))
	fi
	
	# COPY RUN FILE
	# cp /lustre/pfs1/cades-cnms/rbala/Workflow/AddtitionalStrucs/inputFiles/runFiles/run_cdco_pbs.job $i
    done    
}


parse_vasp_output()
{
    inFolderPath=$1
    inFileName=$2
    outFileName=$3
    for i in $(find $inFolderPath -name $inFileName -type f); do
	echo $i
	grep -l 'g(F)' $i
	grep -a 'g(F)' $i|tail -1
	grep -a 'F=' $i|tail -1
	grep -a 'accuracy' $i|tail -1
	grep -a 'BRION' $i|tail -1
	echo	
    done > $inFolderPath/$outFileName
}


test_function()
{
    printf "for vrun in \$(seq 1 \$NUM_RUNS)
	    do
		rundir=\$vrundirs[\$vrun]
		start=\$(expr 1 + \$(expr \$((\$vrun-1)) \* \$CORES_PER_RUN ))
		end=\$(expr \$vrun \* \$CORES_PER_RUN)
		echo \"\$start \$end\"
		awk -v start=\$start -v end=\$end 'NR >= start && NR <= end'  pbs_nodefile > \${rundir}/pbs_nodefile
	    done"
    
    # declare -a arr1=('hello' 'my' 'name' 'is' 'ram')
    # arr2=$(for i in "${arr1[@]}"; do printf "$i"; done)
    # echo "$arr2"
    # for i in ${arr1[@]}; do
    # 	echo $i
    
}

# To enable run the codes inside functions directly from terminal
# bash createApRun.sh func_name arg1 arg2 ... argN
"$@"
