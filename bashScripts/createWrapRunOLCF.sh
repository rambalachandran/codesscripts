#!/bin/bash
## SCRIPT TO ADD folders to wraprun in OLCF (TITAN)
##INPUT
noProcsPerRun=32
binaryName="vasp5"

##PROCESSING
noJobs=0
count=1
echo -en '\n'

echo -n "wraprun --w-roe " >>run.pbs
for i in `find -mindepth 1 -maxdepth 1 -type d`;do
    noJobs=$(($noJobs + $count))
    echo -n " -n $noProcsPerRun --w-cd $i $binaryName : " >>run.pbs;
    yes | cp $i/CONTCAR $i/POSCAR;
done

##OUTPUT
echo "Total No. Jobs" $noJobs
echo "Total No. Procs" $(($noJobs * $noProcsPerRun))
echo "Total No. Nodes" $(($noJobs * $noProcsPerRun / 16))
