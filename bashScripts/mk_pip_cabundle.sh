#!/bin/bash -f

# sfrench 20150618: tmp file name changes for now-shared TMPDIR on edison logins

##mkdir -p $HOME/.pip
##curl http://mxr.mozilla.org/seamonkey/source/security/nss/lib/ckfw/builtins/certdata.txt?raw=1 > $TMPDIR/certdata.txt
##/global/common/shared/usg/bin/mkcabundle.pl $TMPDIR/certdata.txt > $HOME/.pip/cabundle

tmpfile=`mktemp`
mkdir -p $HOME/.pip
curl http://mxr.mozilla.org/seamonkey/source/security/nss/lib/ckfw/builtins/certdata.txt?raw=1 > $tmpfile
/global/common/shared/usg/bin/mkcabundle.pl $tmpfile > $HOME/.pip/cabundle
rm -f $tmpfile
