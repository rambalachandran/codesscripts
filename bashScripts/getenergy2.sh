#!/bin/bash
# argument1 - folderPath
# argument2 - filename
# argument3 - query

#echo folderPath: $1
#echo fileName: $2
#echo query: $3

folderPath=$1
fileName=$2
query=$3

find $folderPath -type f -name $fileName -exec sh -c '
  for f do
    grep TOTEN /dev/null "$f" | tail -n 1    
  done
' sh {} +
