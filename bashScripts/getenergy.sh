#!/bin/bash
# argument1 - folderPath
# argument2 - filename
# argument3 - query

echo folderPath: $1
echo fileName: $2
echo query: $3

folderPath=$1
fileName=$2
query=$3
for i in `find $folderPath -name $fileName -type f`;
do
grep -l $query $i
grep $query $i|tail -1
done
