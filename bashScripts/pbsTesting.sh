#!/bin/bash
#PBS -q debug
#PBS -l mppwidth=8
#PBS -l walltime=00:30:00
#PBS -A m2113

#PBS -o "STDOUT.$PBS_JOBID"
#PBS -j oe
#PBS -m a

#### PBS Options Not Used ####
###PBS -S /bin/bash 
###PBS -V 


# The current vasp version is vasp/5.3.5
#module load vasp
# module load vasp/5.3.5
# module load vasp/5.3.5_vtst

cd $PBS_O_WORKDIR
bash /global/homes/r/rbala/CodesScripts/myCodes/bitbucketRepos/codesscripts/bashScripts/mvPoscarPhonopy.sh 810 >out
