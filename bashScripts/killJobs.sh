#!/bin/bash
# https://gist.github.com/icaoberg/5374558

# jobType can be "R" "Q"

USERNAME=rbala
echo jobType: $1

#to kill all the jobs
#qstat -u$USERNAME | grep "$USERNAME" | cut -d"." -f1 | xargs qdel

#to kill all the running jobs
#qstat -u$USERNAME | grep "R" | cut -d"." -f1 | xargs qdel

#to kill all the queued jobs
qstat -u$USERNAME | grep $1 | cut -d"." -f1 | xargs qdel
