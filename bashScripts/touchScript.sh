#!/bin/sh
## https://unix.stackexchange.com/questions/210425/script-for-changing-modification-time-of-files-and-directories-recursively
for i in "$@"; do
    find "$i"  -exec touch -a -r {} -d '-10 hour' {} \;
done