#!/bin/bash

if (( $# != 1  ))
 then echo "use: delscript.sh filename "
 exit
fi

njobs=$(qstat |grep gpancha |grep $1|wc|awk '{print $1}')
if [ $njobs == 0 ]; then
 echo "No jobs with name $1"
 exit
fi
echo "Number of $1 jobs is: $njobs"
xstart=1;xend=$njobs;xstep=1
for (( x = $xstart; x <= $xend; x += $xstep))
do
num=$(qstat |grep gpancha |grep $1 |head -$x |tail -1 |awk '{print $1}')
qdel $num
done
echo "All $njobs $1 jobs deleted"



