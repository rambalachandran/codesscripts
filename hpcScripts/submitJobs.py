import os, sys, errno
import shutil
from popen2 import popen2
import subprocess
import time
import json
#import pymatgen as mg
from pymatgen.core.structure import Structure
from pymatgen.io.vaspio import Poscar
import numpy as np
from pprint import pprint

# https://stackoverflow.com/questions/229186/os-walk-without-digging-into-directories-below
def walklevel(some_dir, level=1):
    some_dir = some_dir.rstrip(os.path.sep)
    assert os.path.isdir(some_dir)
    num_sep = some_dir.count(os.path.sep)
    for root, dirs, files in os.walk(some_dir):
        yield root, dirs, files
        num_sep_this = root.count(os.path.sep)
        if num_sep + level <= num_sep_this:
            del dirs[:]

# https://stackoverflow.com/questions/354038/how-do-i-check-if-a-string-is-a-number-float-in-python
# TODO: Ask a question about this in stackoverflow
def checkdir(s,str=None):
    if str is not None:
        if str in ('float', 'Float'):
            try:
                float(s)
                return True
            except ValueError:
                return False
        else:
            if str in s:
                return True
            else:
                return False
                
def getSubDirs(path, level, checkVal=None):
    dirNameList = []
    for (dirPath, dirNames, fileNames) in walklevel(path, level):
        for dirVal in dirNames:
            if  checkdir(dirVal, checkVal):
                dirNameList.append(os.path.join(dirPath,dirVal))
    return dirNameList

def createJobScripts(inputData, dirList):
    jobId = []
    for dirVal in dirList:
        #os.chdir(dirVal)
        #output, input = popen2('qsub')
        #subprocess.call('qsub',stdin=input,stdout=subprocess.PIPE)
        job_name = 'VASP-RELAX'
        job_string = """#!/bin/bash
                #PBS -N %s
                #PBS -A %s
                #PBS -l walltime=%s
<<<<<<< HEAD
                #PBS -l nodes=%s
=======
                #PBS -l mppwidth=%s
>>>>>>> origin/master
                #PBS -a %s
                #PBS -j oe
                #PBS -M %s 
                #PBS -m %s
                #PBS -q %s
                module load vasp

<<<<<<< HEAD
                cd $PBS_O_WORKDIR
                aprun -n %s /sw/xk6/vasp5/5.3/sles11.1_intel14.0.2.144/bin/vasp5 > stdout
                checkjob -v $PBS_JOBID >jobinfo
                """ % (job_name, inputData["PBS"]["account"],
                       inputData["PBS"]["walltime"], inputData["PBS"]["totalNodes"],
                       inputData["PBS"]["jobDateTime"],
                       inputData["PBS"]["email_id"], inputData["PBS"]["email_cond"],
                       inputData["PBS"]["queue"], inputData["PBS"]["procs"])
=======
                cd %s
                aprun -n %s vasp > stdout
                checkjob -v $PBS_JOBID >jobinfo
                """ % (job_name, inputData["PBS"]["account"],
                       inputData["PBS"]["walltime"], inputData["PBS"]["procs"],
                       inputData["PBS"]["jobDateTime"],
                       inputData["PBS"]["email_id"], inputData["PBS"]["email_cond"],
                       inputData["PBS"]["queue"],dirVal,
                       inputData["PBS"]["procs"])
>>>>>>> origin/master
                # Send job_string to qsub
        #input.write(job_string)
        #input.close()
        # Print your job and the response to the screen
        # print job_string
        #print 'output', output
        #print output.read()
        #jobId.append(output)
        #time.sleep(2)
        with open(os.path.join(dirVal,'run.pbs'), 'w') as f:
            f.write(job_string)
    return jobId

def  submitHpcJobs(dirList,fileName):
    for dirVal in dirList:
        os.chdir(dirVal)
        bashList = ['qsub',fileName]
        subprocess.call(bashList)
    
if __name__ == '__main__':
<<<<<<< HEAD
    folderPath = '/ccs/home/rbala/myCodes/bitbucketRepos/codesscripts/hpcScripts'
=======
    folderPath = '/global/homes/r/rbala/CodesScripts/myCodes/bitbucketRepos/codesscripts/hpcScripts'
>>>>>>> origin/master
    jsonFile = 'inputSubmitJobs.json'
    with open(os.path.join(folderPath,jsonFile)) as inFile:
        print inFile
        inputData = json.load(inFile)
    '''
    dirNameList = getSubDirs(inputData["inputPath"],inputData["dirLevel"],
                             inputData["subDirCheckVal"])
    with open(os.path.join(inputData["inputPath"],'dirList.txt'),'w') as f:
        for item in dirNameList:
            f.write("%s\n" % item)
    '''
    with open(os.path.join(inputData["inputPath"],'dirList.txt'),'r') as f:
        dirList = f.read().splitlines()
    jobId = createJobScripts(inputData,dirList)
    submitHpcJobs(dirList,'run.pbs')
<<<<<<< HEAD
=======

    
>>>>>>> origin/master
