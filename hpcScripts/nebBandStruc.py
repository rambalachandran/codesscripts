import os, sys
import shutil
from popen2 import popen2
#import subprocess
import time
import json

"""
TO DO
1. Replace popen2 with subprocess module
2. How to print jobids and the corresponding directory location to a file
"""

# Function copied from https://stackoverflow.com/questions/354038/how-do-i-check-if-a-string-is-a-number-float-in-python
def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False


# Function copied from https://stackoverflow.com/questions/229186/os-walk-without-digging-into-directories-below
def walklevel(some_dir, level=1):
    some_dir = some_dir.rstrip(os.path.sep)
    assert os.path.isdir(some_dir)
    num_sep = some_dir.count(os.path.sep)
    for root, dirs, files in os.walk(some_dir):
        yield root, dirs, files
        num_sep_this = root.count(os.path.sep)
        if num_sep + level <= num_sep_this:
            del dirs[:]

            
def submitHpcJobs(inputData, folderNames):
    jobId = []
    for dirVal in folderNames:
        os.chdir(os.path.join(inputData["outputPath"], dirVal))
        output, input = popen2('qsub')
        #subprocess.call('qsub',stdin=input,stdout=subprocess.PIPE)
        job_name = dirVal
        job_string = """#!/bin/bash
                #PBS -N %s
                #PBS -l walltime=%s
                #PBS -l mppwidth=%s
                #PBS -l pmem=%s
                #PBS -a %s
                #PBS -j oe
                #PBS -M %s 
                #PBS -m %s
                #PBS -q %s
                module load vasp
                cd $PBS_O_WORKDIR
                a='jobinfo'
                aprun -n %s gvasp > %s.out
                checkjob -v $PBS_JOBID >$a.$PBS_JOBID
                """ % (job_name, inputData["PBS"]["walltime"], inputData["PBS"]["procs"],
                       inputData["PBS"]["pmem"], inputData["PBS"]["jobDateTime"],
                       inputData["PBS"]["email_id"], inputData["PBS"]["email_cond"],
                       inputData["PBS"]["queue"], inputData["PBS"]["procs"], job_name)
                # Send job_string to qsub
        input.write(job_string)
        input.close()

        # Print your job and the response to the screen
        # print job_string
        print 'output', output
        print 'output.read()', str(output.read())
        jobId.append(str(output.read()))
        time.sleep(2)
        
    return jobId

def createInputFiles(inputData):
    if (inputData["vaspOptions"]["calcType"] == "neb-bandstructure"):
        dirlevel = 0 
        incarFile = os.path.join(inputData["OtherInputPath"],"incarFiles/IBRION_-1_ISMEAR0_HSE")
        kpointsFile = os.path.join(inputData["OtherInputPath"],'kpointsFiles/Gamma_Auto_1x1x1')
        folderNames = []
        for (dirPath, dirNames, fileNames) in walklevel(inputData["PrevDataPath"], dirlevel):
            for dirVal in dirNames:
                if is_number(dirVal):
                    folderNames.append(dirVal)

        print 'Subdirectories are', folderNames
        
        for dirVal in folderNames:
                print 'Subdirectory', dirVal
                inFolder = os.path.join(inputData["PrevDataPath"], dirVal)
                potcarFile = os.path.join(inputData["PrevDataPath"],'POTCAR')
                contcarFile = os.path.join(inFolder, 'CONTCAR')
                poscarFile = os.path.join(inFolder, 'POSCAR')

                outFolder = os.path.join(inputData["outputPath"], dirVal)
                if os.path.isdir(outFolder):
                    print 'Subfolder', outFolder, 'exists'
                else:
                    os.mkdir(outFolder)

                shutil.copyfile(incarFile, os.path.join(outFolder, 'INCAR'))
                shutil.copyfile(potcarFile, os.path.join(outFolder, 'POTCAR'))
                shutil.copyfile(kpointsFile, os.path.join(outFolder, 'KPOINTS'))

                if os.path.isfile(contcarFile):
                    shutil.copyfile(contcarFile,os.path.join(outFolder, 'POSCAR'))
                else:
                    print 'CONTCAR Missing in folder ', inFolder,
                    'Copying POSCAR'
                    shutil.copyfile(poscarFile,os.path.join(outFolder, 'POSCAR'))
        
        return folderNames
    else:
        print 'Calculations other than neb-bandstructure are not implemented yet'
        
            
        
if __name__ == '__main__':
    folderPath = '/global/homes/r/rbala/CodesScripts/myCodes/bitbucketRepos/codesscripts/hpcScripts'
    jsonFile = 'inputNebBandStruc.json'
    with open(os.path.join(folderPath,jsonFile)) as inFile:
        inputData = json.load(inFile)
    if os.path.isdir(inputData["outputPath"]):
        print "OutputPath", inputData["outputPath"], "EXISTS"
    else:
        os.mkdir(inputData["outputPath"])

    folderNames = createInputFiles(inputData)
    jobId = submitHpcJobs(inputData, folderNames)
    print 'JobIds are', jobId
    with open(os.path.join(inputData["outputPath"],inputData["jobIdFile"]), 'w') as jobIdFile:
        for item in jobId:
            jobIdFile.write("%s\n" % item)
            
       
