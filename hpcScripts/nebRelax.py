import os, sys, errno
import shutil
from popen2 import popen2
#import subprocess
import time
import json
#import pymatgen as mg
from pymatgen.core.structure import Structure
from pymatgen.io.vaspio import Poscar
import numpy as np

"""
Cluster
Oindex1: 66 (dir: [0,1,0])
Oindex2: 93 (dir: [1,0,0])
dopantIndex: 47

NoCluster
Oindex1: 78 (dir: [0,-1,0])
Oindex2: 102 (dir: [1,0,0])
dopantIndex: 53

# Create POSCAR by adding H atoms to the converged dry POSCAR
    # Create vasp inputFiles and folders for full relaxation & SD
    # Submit HPC jobs
    # Create vasp inputFiles & folders for NEB calculation
    # Submit HPC jobs
    # Create vasp inputFiles & folders for neb-bandstructure calculation
    # Submit HPC jobs
"""
#### TODO
# 1. Replace popen2 with subprocess module
# 2. How to print jobids and the corresponding directory location to a file
# 3. Fix create_path function to makesure that it can take arbitrary no. of arguments
####


# https://stackoverflow.com/questions/600268/mkdir-p-functionality-in-python
def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc: # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else: raise

# https://stackoverflow.com/questions/354038/how-do-i-check-if-a-string-is-a-number-float-in-python
# TODO: Ask a question about this in stackoverflow
def checkdir(s,str=None):
    if str is not None:
        if str in ('float', 'Float'):
            try:
                float(s)
                return True
            except ValueError:
                return False
        else:
            if str in s:
                return True
            else:
                return False
                
# https://stackoverflow.com/questions/229186/os-walk-without-digging-into-directories-below
def walklevel(some_dir, level=1):
    some_dir = some_dir.rstrip(os.path.sep)
    assert os.path.isdir(some_dir)
    num_sep = some_dir.count(os.path.sep)
    for root, dirs, files in os.walk(some_dir):
        yield root, dirs, files
        num_sep_this = root.count(os.path.sep)
        if num_sep + level <= num_sep_this:
            del dirs[:]

def create_path(path, listVal=0):
    try:
        len(listVal)
        for item in listVal:
            itemPath = os.path.join(path, item)
            if (os.path.isdir(itemPath) or os.path.isfile(itemPath)):
                break
        return itemPath
    except TypeError:
        return os.path.join(path)
            
def submitHpcJobs(inputData, dirList):
    jobId = []
    for dirVal in dirList:
        os.chdir(dirVal)
        output, input = popen2('qsub')
        #subprocess.call('qsub',stdin=input,stdout=subprocess.PIPE)
        job_name = dirVal
        job_string = """#!/bin/bash
                #PBS -N %s
                #PBS -A %s
                #PBS -l walltime=%s
                #PBS -l mppwidth=%s
                #PBS -l pmem=%s
                #PBS -a %s
                #PBS -j oe
                #PBS -M %s 
                #PBS -m %s
                #PBS -q %s
                module load vasp
                cd $PBS_O_WORKDIR
                a='jobinfo'
                aprun -n %s vasp > %s.out
                checkjob -v $PBS_JOBID >$a.$PBS_JOBID
                """ % (job_name, inputData["PBS"]["account"],
                       inputData["PBS"]["walltime"], inputData["PBS"]["procs"],
                       inputData["PBS"]["pmem"], inputData["PBS"]["jobDateTime"],
                       inputData["PBS"]["email_id"], inputData["PBS"]["email_cond"],
                       inputData["PBS"]["queue"], inputData["PBS"]["procs"], job_name)
                # Send job_string to qsub
        input.write(job_string)
        input.close()
        # Print your job and the response to the screen
        # print job_string
        #print 'output', output
        print output.read()
        jobId.append(output)
        time.sleep(2)
        
    return jobId


def submitHpcJobsV2(inputData, dirList):
    jobId = []
    output, input = popen2('qsub')
    job_name = "VASP-RELAX"
    job_string = """ 
                 #!/bin/bash
                 #PBS -N %s
                 #PBS -A %s
                 #PBS -l walltime=%s
                 #PBS -l nodes=%s
                 #PBS -a %s
                 #PBS -j oe
                 #PBS -M %s 
                 #PBS -m %s
                 #PBS -q %s
                 module load vasp5/5.3
                 """ % (job_name, inputData["PBS"]["account"],
                        inputData["PBS"]["walltime"], 
                        inputData["PBS"]["totalNodes"], inputData["PBS"]["jobDateTime"],
                        inputData["PBS"]["email_id"], inputData["PBS"]["email_cond"],
                        inputData["PBS"]["queue"])

    for cnt,dirVal in enumerate(dirList):

        tmpstring = """ 
                    cd %s
                    aprun -n %s /sw/xk6/vasp5/5.3/sles11.1_intel14.0.2.144/bin/vasp5 > %s & 
                    """ % (dirVal,
                           inputData["PBS"]["procsPerAprun"],"stdout")
        if (cnt <100):
            job_string = "\n".join([job_string, tmpstring])
        #print tmpstring
    tmpstring = """ wait """
    job_string = "\n".join([job_string, tmpstring])
    input.write(job_string)
    input.close()
    print output.read()
    jobId.append(output)
    pbsFileName = os.path.join(inputData["outputPath"],"run.pbs")
    with open(pbsFileName, 'w') as outFile:
        outFile.write(job_string)
    return jobId

# TODO - HOW to loop through different values of incarFile,poscarFile etc -- when they become list of decreasing priority
# TODO - Create classes for interstitial and base atoms and read the whole dictionary from json files to create instances (objects) of these classes. Follow pymatgen species class for implementation.

def createInputFiles(inputData, dirNameList):
    outDirList = []
    for dirVal in dirNameList:
        print 'Dopant Type', dirVal
        if (inputData["calcOptions"]["calcType"] in ('3D_relax_interstitial')):
            inFolder = os.path.join(inputData["inputPath"], dirVal)
            incarFile = os.path.join(inputData["OtherInputPath"],
                                     "incarFiles/IBRION_2_ISMEAR_-5")
            kpointsFile = os.path.join(inFolder,inputData["vaspFiles"]["kpointsFile"])
            #incarFile = create_path(inFolder, inputData["vaspFiles"]["incarFile"]
            poscarFile_in = create_path(inFolder, inputData["vaspFiles"]["poscarFile"]) #EXTEND IT TO TAKE ARBITRARY NO. OF ARGUMENTS like os.path.join
            potcarFile_in = os.path.join(inFolder, inputData["vaspFiles"]["potcarFile"])
            potcarFile_intst = os.path.join(inputData["OtherInputPath"],
                                            "potcarFiles", inputData["calcOptions"]["xcFunctional"],
                                            inputData["calcOptions"]["pseudopotential"],"POTCAR")
            intstAtom = inputData["calcOptions"]["intstAtom"]["type"]
            noIntst = inputData["calcOptions"]["intstAtom"]["num"]
            indexList = inputData["calcOptions"]["baseAtom"]["index"]
            bondLength = inputData["calcOptions"]["bondLength"]
            
            for (cnt,indexVal) in enumerate(indexList):
                if inputData["calcOptions"]["sdFlag"]:
                    outFolder = os.path.join(inputData["outputPath"], dirVal,
                                             str(indexVal)+'SD')
                else:
                    outFolder = os.path.join(inputData["outputPath"], dirVal,
                                             str(indexVal))
                if not os.path.isdir(outFolder): #TODO - Move this check to mkdir_p
                    mkdir_p(outFolder)
                outDirList.append(outFolder)
                
                potcarFile_names = [potcarFile_in, potcarFile_intst]
                potcarFile = os.path.join(outFolder, 'POTCAR')
                with open(potcarFile, 'w') as outfile:
                    for fname in potcarFile_names:
                        with open(fname) as infile:
                            for line in infile:
                                outfile.write(line)

                crystalStruc =  Structure.from_file(poscarFile_in)
                vec = inputData["calcOptions"]["baseAtom"]["vec"][cnt]
                intstCoords = crystalStruc[indexVal].coords + bondLength*np.asarray(vec)
                crystalStruc.append(intstAtom,intstCoords,True) #TODO: Extend it for multiple interstitial atoms
                atomCnt = len([x for x in crystalStruc])

                if inputData["calcOptions"]["sdFlag"]:
                    sdArray = [[False,False,False]]*(atomCnt)
                    for i in range(1, noIntst+1):
                        sdArray[atomCnt-i] = [True, True, True]
                    poscarFile = Poscar(crystalStruc,'intst',sdArray)
                else:
                    poscarFile = Poscar(crystalStruc,'intst')
                    
                #shutil.copyfile(poscarFile, os.path.join(outFolder, 'POSCAR'))
                # shutil.copyfile(potcarFile, os.path.join(outFolder, 'POTCAR'))
                poscarFile.write_file(os.path.join(outFolder, 'POSCAR'))
                shutil.copyfile(incarFile, os.path.join(outFolder, 'INCAR'))
                shutil.copyfile(kpointsFile, os.path.join(outFolder, 'KPOINTS'))
    return outDirList
        
def getSubDirs(path, level, checkVal):
    dirNameList = []
    for (dirPath, dirNames, fileNames) in walklevel(path, level):
        for dirVal in dirNames:
            if checkdir(dirVal, checkVal):
                dirNameList.append(dirVal)
    return dirNameList
                
if __name__ == '__main__':
    folderPath = '/ccs/home/rbala/myCodes/bitbucketRepos/codesscripts/hpcScripts'
    jsonFile = 'inputVaspCalcV2.json'
    with open(os.path.join(folderPath,jsonFile)) as inFile:
        print inFile
        inputData = json.load(inFile)
    if os.path.isdir(inputData["outputPath"]):
        print "OutputPath", inputData["outputPath"], "EXISTS"
    else:
        mkdir_p(inputData["outputPath"])

    dirNameList = getSubDirs(inputData["inputPath"],inputData["dirLevel"],
                             inputData["subDirCheckVal"])
    outDirList = createInputFiles(inputData, dirNameList)
    #print outDirList
    
    jobId = submitHpcJobsV2(inputData, outDirList)
    #print 'JobIds are', jobId
    with open(os.path.join(inputData["outputPath"], inputData["jobIdFile"]), 'w') as jobIdFile:
        for output in jobId:
            jobIdFile.write(output.read())
    
