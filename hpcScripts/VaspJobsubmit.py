import os, sys
import shutil
from popen2 import popen2
import time
#from os import walk

####### TO DO STUFF
## Create 3 separate classes for job submission, job monitoring & post processing
## Read inputs from an input file through XML MINIDOM

### JOB SUBMISSION
## 1. Implement logic to (i) give input foldernames, (ii) depth of output folders, (iii) which input folder needs to be in which level & (iv) what name should we copy the input files to be.
## 2. Write the job id's into a list and into a file

### JOB MONITORING & RESTART
## 1. Implement Brock Palen's e-mail logic to identify if a job has completed, in queuer or running.
## 2. If completed, come up with logic to see if the job ran successfully (both as defined by queue & by the ab-initio software) or if it failed with error (I think STOP in .o file is a good check)
## 3. Think about strategies to restart failed jobs (come up with checks that cause most of the common job failures)

### Post Processing
## 1. Implement import_doscar in python, parse doscar & plot the results and save.
## 2. Parse outcar to get free energy (& its components), forces (& its components in relaxation), convergence tol, fermi energy
## 3. Geting provenance data for the executable (from other codes like sumatra)
## 4. Think about how to store all the data (in what format) that makes it easy to retrieve.

inputFileFolder = "/scratch/djsiege_flux/janarcs/postDocProjects/solventStability/MgO_THF/EqDistance/MgO_THF-Ver_OverMg/InputFiles"
outputDir = "/scratch/djsiege_flux/janarcs/postDocProjects/solventStability/MgO_THF/EqDistance/MgO_THF-Ver_OverMg/OutputFiles"
#potcarFilePath = "/global/homes/r/rbala/myDocuments/VaspStuff/potcarFiles/PAW_PBE_C_H_Mg_O"
vdwFilePath = "/home/janarcs/myDocuments/VaspStuff/vdw_kernel.bindat"
# pbsFilePath = "/global/homes/r/rbala/myDocuments/run.pbs"
vdwFlag= True


# Check if inputdirectory exists - if not throw an error and stop program

if os.path.isdir(outputDir):
    print outputDir, "EXISTS"
else:
    os.mkdir(outputDir)

inputFolderList = [ ]
for (dirpath, dirnames, filenames) in os.walk(inputFileFolder):
    inputFolderList.extend(dirnames)
print inputFolderList

poscarFileList = [ ]
incarFileList = [ ]
kpointFileList = [ ]
potcarFileList = [ ]

for folderName in inputFolderList:
    folderPath =  os.path.join(inputFileFolder, folderName)
    for (dirpath, dirnames, filenames) in os.walk(folderPath):
        if (folderName == "PoscarFiles"):
            poscarFileList.extend(filenames)        
        elif (folderName == "KpointFiles"):
            kpointFileList.extend(filenames)
        elif (folderName == "IncarFiles"):
            incarFileList.extend(filenames)
        elif (folderName == "PotcarFiles"):
            potcarFileList.extend(filenames)

print poscarFileList
print kpointFileList
print incarFileList
print potcarFileList

# the filenames in PoscarFiles folder Level1 subfolders 
# the filenames in KpointFiles & IncarFiles are concatenated to create Level2 subfolders

for fileName in poscarFileList:
    folderLev1_path = os.path.join(outputDir, fileName)
    if os.path.isdir(folderLev1_path):
        print folderLev1_path, "EXISTS"
    else:
        os.mkdir(folderLev1_path)
    for fileName2 in incarFileList:
        for fileName3 in kpointFileList:
            for fileName4 in potcarFileList:
                folderLev2_name=fileName2+"_"+fileName3+"_"+fileName4
                folderLev2_path = os.path.join(folderLev1_path,folderLev2_name)
                if os.path.isdir(folderLev2_path):
                    print folderLev2_path, "EXISTS"
                else:
                    os.mkdir(folderLev2_path)

print "Folders Are Created"

#### Copying Files to directories and submitting jobs

poscarFolderPath=os.path.join(inputFileFolder,"PoscarFiles")
incarFolderPath=os.path.join(inputFileFolder,"IncarFiles")
kpointFolderPath=os.path.join(inputFileFolder,"KpointFiles")
potcarFolderPath=os.path.join(inputFileFolder,"PotcarFiles")

#pbsJobId = [ ] #Create a list of pbs job id and then save it to file.

for fileName in poscarFileList:
    poscarFilePath=os.path.join(poscarFolderPath,fileName)
    for fileName2 in incarFileList:
        incarFilePath=os.path.join(incarFolderPath,fileName2)
        for fileName3 in kpointFileList:    
            kpointFilePath=os.path.join(kpointFolderPath,fileName3)
            for fileName4 in potcarFileList:
                potcarFilePath=os.path.join(potcarFolderPath,fileName4)
                outFolderName=fileName+"/"+fileName2+"_"+fileName3+"_"+fileName4
                outFolderPath = os.path.join(outputDir,outFolderName)
                shutil.copyfile(poscarFilePath, outFolderPath+"/"+"POSCAR")
                shutil.copyfile(incarFilePath, outFolderPath+"/"+"INCAR")
                shutil.copyfile(kpointFilePath, outFolderPath+"/"+"KPOINTS")
                shutil.copyfile(potcarFilePath, outFolderPath+"/"+"POTCAR")
                if vdwFlag:
                    shutil.copyfile(vdwFilePath, outFolderPath+"/"+"vdw_kernel.bindat")

                # Open a pipe to the qsub command.
                os.chdir(outFolderPath)
                output, input = popen2('qsub')

                # Customize your options here
                # If you want to be emailed by the system, include these in job_string:
                #PBS -M your_email@address
                #PBS -m abe # (a = abort, b = begin, e = end)

                job_name = fileName+"_"+fileName2  
                jobDateTime =  "201501111500"                
                walltime = "06:00:00"
                procs ="16"
                pmem = "3900mb"
                queue = "flux"
                #queue = "cac"
                qos = "flux"
                #qos = "vikramg"
                #info = "#PBS -A prismsprojectdebug_flux"
                info = "#PBS -A djsiege_flux"
                #info = " #PBS -l feature=ibgroup4"
                email_id = "janarcs@umich.edu"
                email_cond = "e"
                job_string = """
                #!/bin/bash
                #PBS -S /bin/sh
                #PBS -N %s
                #PBS -V
                #PBS -j oe
                #PBS -l walltime=%s
                #PBS -l procs=%s
                #PBS -l pmem=%s
                #PBS -a %s
                %s
                #PBS -l qos=%s
                #PBS -q %s
                #PBS -M %s 
                #PBS -m %s
                cd $PBS_O_WORKDIR
                a='jobinfo'
                mpirun ~/vasp.130403 > %s.out
                checkjob -v $PBS_JOBID >$a.$PBS_JOBID
                """ % (job_name, walltime, procs, pmem, jobDateTime, info, qos, queue, email_id, email_cond, job_name)
                # Send job_string to qsub
                input.write(job_string)
                input.close()

                # Print your job and the response to the screen
                # print job_string
                print output.read()
                time.sleep(2)

print "Files Copied & Job Submitted"


### Submitting cluster jobs
#PBS -M your_email@address
#PBS -m abe  # (a = abort, b = begin, e = end)

