import os, sys, errno
import shutil
from popen2 import popen2
import subprocess
import time
import json
#import pymatgen as mg
from pymatgen.core.structure import Structure
from pymatgen.io.vaspio import Poscar
import numpy as np
from pprint import pprint
import re
import matplotlib.pyplot as plt
import csv
import copy
"""
Cluster
Oindex1: 66 (dir: [0,1,0])
Oindex2: 93 (dir: [1,0,0])
dopantIndex: 47

NoCluster
Oindex1: 78 (dir: [0,-1,0])
Oindex2: 102 (dir: [1,0,0])
dopantIndex: 53

# Create POSCAR by adding H atoms to the converged dry POSCAR
    # Create vasp inputFiles and folders for full relaxation & SD
    # Submit HPC jobs
    # Create vasp inputFiles & folders for NEB calculation
    # Submit HPC jobs
    # Create vasp inputFiles & folders for neb-bandstructure calculation
    # Submit HPC jobs
"""
#### TODO
# 1. Replace popen2 with subprocess module
# 2. How to print jobids and the corresponding directory location to a file
# 3. Fix create_path function to makesure that it can take arbitrary no. of arguments
####

### TMP CORRECTION FOR INCAR START ###
'''
    incarFile_in = os.path.join(inputData["OtherInputPath"],
                        "incarFiles/IBRION_2_ISMEAR_0_NEB")
    for dirVal in dirList:
        incarFile_out = os.path.join(dirVal,'INCAR')
        shutil.copyfile(incarFile_in, incarFile_out)
        with open(incarFile_out,'a') as myfile:
            myfile.write("IMAGES = %d"
                         %(inputData["INCAR"]["IMAGES"]))
'''
#### TEMP CORRECTION FOR INCAR END ###

# https://stackoverflow.com/questions/600268/mkdir-p-functionality-in-python
def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc: # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else: raise

# https://stackoverflow.com/questions/354038/how-do-i-check-if-a-string-is-a-number-float-in-python
# TODO: Ask a question about this in stackoverflow
def checkdir(s,str=None):
    if str is not None:
        if str in ('float', 'Float'):
            try:
                float(s)
                return True
            except ValueError:
                return False
        else:
            if str in s:
                return True
            else:
                return False
                
# https://stackoverflow.com/questions/229186/os-walk-without-digging-into-directories-below
def walklevel(some_dir, level=1):
    some_dir = some_dir.rstrip(os.path.sep)
    assert os.path.isdir(some_dir)
    num_sep = some_dir.count(os.path.sep)
    for root, dirs, files in os.walk(some_dir):
        yield root, dirs, files
        num_sep_this = root.count(os.path.sep)
        if num_sep + level <= num_sep_this:
            del dirs[:]

def create_path(path, listVal=0):
    try:
        len(listVal)
        for item in listVal:
            itemPath = os.path.join(path, item)
            if (os.path.isdir(itemPath) or os.path.isfile(itemPath)):
                break
        return itemPath
    except TypeError:
        return os.path.join(path)
'''            
def submitHpcJobs(inputData, dirList):
    jobId = []
    for dirVal in dirList:
        os.chdir(dirVal)
        output, input = popen2('qsub')
        #subprocess.call('qsub',stdin=input,stdout=subprocess.PIPE)
        job_name = dirVal
        job_string = """#!/bin/bash
                #PBS -N %s
                #PBS -l walltime=%s
                #PBS -l mppwidth=%s
                #PBS -l pmem=%s
                #PBS -a %s
                #PBS -j oe
                #PBS -M %s 
                #PBS -m %s
                #PBS -q %s
                module load vasp
                cd $PBS_O_WORKDIR
                a='jobinfo'
                aprun -n %s vasp > %s.out
                checkjob -v $PBS_JOBID >$a.$PBS_JOBID
                """ % (job_name, inputData["PBS"]["walltime"], inputData["PBS"]["procs"],
                       inputData["PBS"]["pmem"], inputData["PBS"]["jobDateTime"],
                       inputData["PBS"]["email_id"], inputData["PBS"]["email_cond"],
                       inputData["PBS"]["queue"], inputData["PBS"]["procs"], job_name)
                # Send job_string to qsub
        input.write(job_string)
        input.close()
        # Print your job and the response to the screen
        # print job_string
        #print 'output', output
        print output.read()
        jobId.append(output)
        time.sleep(2)
    return jobId
'''

def submitHpcJobsEndpts(inputData, dirList):
    jobId = []
    job_name = 'NEB-Endpts-Static'
    jobStringBase = """#!/bin/bash
                #PBS -N %s
                #PBS -l walltime=%s
                #PBS -l mppwidth=%s
                #PBS -l pmem=%s
                #PBS -A %s
                #PBS -j oe
                #PBS -M %s 
                #PBS -m %s
                #PBS -q %s
                module load vasp
                """ % (job_name, inputData["PBS"]["walltime"], inputData["PBS"]["procsTotal"],
                       inputData["PBS"]["pmem"], inputData["PBS"]["account"],
                       inputData["PBS"]["email_id"], inputData["PBS"]["email_cond"],
                       inputData["PBS"]["queue"])
    endPts = ['00', '0'+str(inputData["INCAR"]["IMAGES"]+1)]
 
    for dirVal in dirList:
        for val in endPts:
            localPath = os.path.join(dirVal,val)
            os.chdir(localPath)
            output, input = popen2('qsub')
            
            ###endPt1_dir = os.path.join(dirVal,endPt1)
            ###endPt2_dir = os.path.join(dirVal,endPt2)
            ###subprocess.call('qsub',stdin=input,stdout=subprocess.PIPE)
            tmpstring = """
                       cd %s
                       aprun -n %s gvasp > %s
                       checkjob -v $PBS_JOBID >jobinfo
                       """%(localPath, inputData["PBS"]["procsPerAprun"], 'stdout')
            job_string = "\n".join([jobStringBase, tmpstring])
            input.write(job_string)
            input.close()
            print output.read()
            jobId.append(output)
            time.sleep(2)
            pbsFileName = os.path.join(dirVal,val,"runStatic.pbs")
            with open(pbsFileName, 'w') as outFile:
                outFile.write(job_string)
    return jobId


def submitHpcJobsNebCalc(inputData, dirList):
    jobId = []
    job_name = 'NEB-Endpts-Static'
    jobStringBase = """#!/bin/bash
                #PBS -N %s
                #PBS -l walltime=%s
                #PBS -l mppwidth=%s
                #PBS -l pmem=%s
                #PBS -A %s
                #PBS -j oe
                #PBS -M %s 
                #PBS -m %s
                #PBS -q %s
                module load vasp/5.3.3_vtst-pgi
                """ % (job_name, inputData["PBS"]["walltime"], inputData["PBS"]["procsTotal"],
                       inputData["PBS"]["pmem"], inputData["PBS"]["account"],
                       inputData["PBS"]["email_id"], inputData["PBS"]["email_cond"],
                       inputData["PBS"]["queue"])
 
    for dirVal in dirList:
        os.chdir(dirVal)
        output, input = popen2('qsub')
            
        ###subprocess.call('qsub',stdin=input,stdout=subprocess.PIPE)
        tmpstring = """
                    cd %s
                    aprun -n %s gvasp > %s
                    checkjob -v $PBS_JOBID >jobinfo
                    """%(dirVal, inputData["PBS"]["procsPerAprun"], 'stdout')
        job_string = "\n".join([jobStringBase, tmpstring])
        input.write(job_string)
        input.close()
        print output.read()
        jobId.append(output)
        time.sleep(2)
        pbsFileName = os.path.join(dirVal,"run.pbs")
        with open(pbsFileName, 'w') as outFile:
            outFile.write(job_string)
    return jobId

def submitHpcJobsNebSP(inputData, dirList):
    incarFile = os.path.join(inputData["OtherInputPath"],
                             "incarFiles/IBRION_-1_ISMEAR_0_EDIFF_1E-08")
    kpointsFile = os.path.join(inputData["OtherInputPath"],
                               "kpointsFiles/Gamma_Auto_1x1x1")
    jobId = []
    job_name = 'NEB-Static'
    jobStringBase = """#!/bin/bash
                #PBS -N %s
                #PBS -l walltime=%s
                #PBS -l mppwidth=%s
                #PBS -l pmem=%s
                #PBS -A %s
                #PBS -j oe
                #PBS -M %s 
                #PBS -m %s
                #PBS -q %s
                module load vasp
                """ % (job_name, inputData["PBS"]["walltime"], inputData["PBS"]["procsTotal"],
                       inputData["PBS"]["pmem"], inputData["PBS"]["account"],
                       inputData["PBS"]["email_id"], inputData["PBS"]["email_cond"],
                       inputData["PBS"]["queue"])
    endPts = []
    for i in range(0, inputData["INCAR"]["IMAGES"]+2):
        endPts.append('0'+str(i))  ##TO RUN OUTLIERS
    #endPts.append('06')
    for dirVal in dirList:
        potcarFile = os.path.join(dirVal,inputData["vaspFiles"]["potcarFile"])
        for val in endPts:
            localPath = os.path.join(dirVal,val)
            os.chdir(localPath)
            shutil.copyfile('POSCAR','POSCAR_01')
            shutil.copyfile('CONTCAR','POSCAR')
            shutil.copyfile(incarFile, 'INCAR')
            shutil.copyfile(potcarFile, 'POTCAR')
            shutil.copyfile(kpointsFile, 'KPOINTS')
            output, input = popen2('qsub')
            tmpstring = """
                       cd %s
                       aprun -n %s gvasp > %s
                       checkjob -v $PBS_JOBID >jobinfo
                       """%(localPath, inputData["PBS"]["procsPerAprun"], 'stdout')
            job_string = "\n".join([jobStringBase, tmpstring])
            input.write(job_string)
            input.close()
            print output.read()
            jobId.append(output)
            #time.sleep(2)
            pbsFileName = os.path.join(dirVal,val,"runStatic.pbs")
            with open(pbsFileName, 'w') as outFile:
                outFile.write(job_string)
    return jobId


def parseTransitionEnergy(inputData,dirList):
    for dirVal in dirList:
        #os.chdir(dirVal)
        energyFileTemp =  os.path.join(dirVal, inputData["Misc"]["energyFileTemp"])
        shellInput = [inputData["Misc"]["energyscript"], dirVal,
                      inputData["Misc"]["energySearchFile"],
                      inputData["Misc"]["energyQuery"]]
        f = open(energyFileTemp, 'w')
        subprocess.call(shellInput,stdout=f)
        f.close()


def accumulateTransitionEnergy(inputData,dirList):
    transitionEnergyList = []
    for dirVal in dirList:
        #dopant = re.split('/|_',dirVal)[12]
        #endPt1_id = int(re.split('/|_',dirVal)[13])
        #endPt2_id = int(re.split('/|_',dirVal)[14])
        systemInfo = re.split('/|_',dirVal)[12:15]
        energyList = []
        energyFileTemp =  os.path.join(dirVal, inputData["Misc"]["energyFileTemp"])
        with open(energyFileTemp, 'r') as inFile:
            for line in inFile:
                lineVal = line.split()
                freeEnergy = float(lineVal[5])
                imageNo = int(re.split('/',lineVal[0])[-2])
                energyList.append([imageNo,freeEnergy])
        energyList.sort(key=lambda val:val[0])
        energyFileName = os.path.join(dirVal, inputData["Misc"]["energyFileName"])
        with open(energyFileName,'w') as outFile:
            writer = csv.writer(outFile)
            writer.writerow(['Image-No','Free-Energy(eV)'])
            writer.writerows(energyList)

        transitionStateNo = int(round(inputData["INCAR"]["IMAGES"]/2.0))
        transitionEnergy = energyList[transitionStateNo][1] - energyList[0][1]
        #tmpInfo = copy.copy(systemInfo) #NOT WORKING
        tmpInfo = list(systemInfo)
        tmpInfo.append(str(transitionEnergy))
        print transitionEnergy, str(transitionEnergy), systemInfo, tmpInfo #TESTING
        transitionEnergyList.append(tmpInfo)

    return transitionEnergyList
        
# TODO - HOW to loop through different values of incarFile,poscarFile etc -- when they become list of decreasing priority
# TODO - Create classes for interstitial and base atoms and read the whole dictionary from json files to create instances (objects) of these classes. Follow pymatgen species class for implementation.

def createRelaxInputFiles(inputData, dirNameList):
    outDirList = []
    for dirVal in dirNameList:
        print 'Dopant Type', dirVal
        if (inputData["calcOptions"]["calcType"] in ('3D_relax_interstitial')):
            inFolder = os.path.join(inputData["inputPath"], dirVal)
            incarFile = os.path.join(inputData["OtherInputPath"],
                                     "incarFiles/IBRION_2_ISMEAR_-5")
            kpointsFile = os.path.join(inputData["OtherInputPath"],
                                       "kpointsFiles/Gamma_Auto_1x1x1")
            #kpointsFile = os.path.join(inFolder,inputData["vaspFiles"]["kpointsFile"])
            #incarFile = create_path(inFolder, inputData["vaspFiles"]["incarFile"]
            poscarFile_in = create_path(inFolder, inputData["vaspFiles"]["poscarFile"]) #EXTEND IT TO TAKE ARBITRARY NO. OF ARGUMENTS like os.path.join
            potcarFile_in = os.path.join(inFolder, inputData["vaspFiles"]["potcarFile"])
            potcarFile_intst = os.path.join(inputData["OtherInputPath"],
                                            "potcarFiles", inputData["calcOptions"]["xcFunctional"],
                                            inputData["calcOptions"]["pseudopotential"],"POTCAR")
            intstAtom = inputData["calcOptions"]["intstAtom"]["type"]
            noIntst = inputData["calcOptions"]["intstAtom"]["num"]
            indexList = inputData["calcOptions"]["baseAtom"]["index"]
            bondLength = inputData["calcOptions"]["bondLength"]
            
            for (cnt,indexVal) in enumerate(indexList):
                if inputData["calcOptions"]["sdFlag"]:
                    outFolder = os.path.join(inputData["outputPath"], dirVal,
                                             str(indexVal)+'SD')
                else:
                    outFolder = os.path.join(inputData["outputPath"], dirVal,
                                             str(indexVal))
                if not os.path.isdir(outFolder): #TODO - Move this check to mkdir_p
                    mkdir_p(outFolder)
                outDirList.append(outFolder)
                
                potcarFile_names = [potcarFile_in, potcarFile_intst]
                potcarFile = os.path.join(outFolder, 'POTCAR')
                with open(potcarFile, 'w') as outfile:
                    for fname in potcarFile_names:
                        with open(fname) as infile:
                            for line in infile:
                                outfile.write(line)

                crystalStruc =  Structure.from_file(poscarFile_in)
                vec = inputData["calcOptions"]["baseAtom"]["vec"][cnt]
                intstCoords = crystalStruc[indexVal].coords + bondLength*np.asarray(vec)
                crystalStruc.append(intstAtom,intstCoords,True) #TODO: Extend it for multiple interstitial atoms
                atomCnt = len([x for x in crystalStruc])

                if inputData["calcOptions"]["sdFlag"]:
                    sdArray = [[False,False,False]]*(atomCnt)
                    for i in range(1, noIntst+1):
                        sdArray[atomCnt-i] = [True, True, True]
                    poscarFile = Poscar(crystalStruc,'intst',sdArray)
                else:
                    poscarFile = Poscar(crystalStruc,'intst')
                    
                #shutil.copyfile(poscarFile, os.path.join(outFolder, 'POSCAR'))
                # shutil.copyfile(potcarFile, os.path.join(outFolder, 'POTCAR'))
                poscarFile.write_file(os.path.join(outFolder, 'POSCAR'))
                shutil.copyfile(incarFile, os.path.join(outFolder, 'INCAR'))
                shutil.copyfile(kpointsFile, os.path.join(outFolder, 'KPOINTS'))
    return outDirList

def createNebInputFiles(inputData, dirNameList):
    outDirList = []
    for dirVal in dirNameList:
        print 'Dopant Type', dirVal
        if (inputData["calcOptions"]["calcType"] in ('Neb_Calc')):
            inFolder = os.path.join(inputData["inputPath"], dirVal)
            incarFile_in = os.path.join(inputData["OtherInputPath"],
                                     "incarFiles/IBRION_2_ISMEAR_0_NEB")
            kpointsFile_in = os.path.join(inputData["OtherInputPath"],
                                       "kpointsFiles/Gamma_Auto_1x1x1")
            indexList = inputData["calcOptions"]["baseAtom"]["index"]
            if inputData["calcOptions"]["sdFlag"]:
                poscarFile_in1 = os.path.join(inFolder, str(indexList[0])+'SD', inputData["vaspFiles"]["poscarFile"])
                poscarFile_in2 = os.path.join(inFolder, str(indexList[1])+'SD', inputData["vaspFiles"]["poscarFile"])
                #kpointsFile_in = os.path.join(inFolder, str(indexList[0])+'SD', inputData["vaspFiles"]["kpointsFile"])
                potcarFile_in = os.path.join(inFolder, str(indexList[0])+'SD', inputData["vaspFiles"]["potcarFile"])
                outFolder = os.path.join(inputData["outputPath"],dirVal,
                                         str(indexList[0])+'_'+str(indexList[1])+'SD')
            else:
                poscarFile_in1 = os.path.join(inFolder, str(indexList[0]), inputData["vaspFiles"]["poscarFile"])
                poscarFile_in2 = os.path.join(inFolder, str(indexList[1]), inputData["vaspFiles"]["poscarFile"])
                #kpointsFile_in = os.path.join(inFolder, str(indexList[0]), inputData["vaspFiles"]["kpointsFile"])
                potcarFile_in = os.path.join(inFolder, str(indexList[0]), inputData["vaspFiles"]["potcarFile"])
                outFolder = os.path.join(inputData["outputPath"],dirVal,
                                         str(indexList[0])+'_'+str(indexList[1]))

            if not os.path.isdir(outFolder): #TODO - Move this check to mkdir_p
                mkdir_p(outFolder)
            outDirList.append(outFolder)
            incarFile_out = os.path.join(outFolder, 'INCAR')
            shutil.copyfile(incarFile_in, incarFile_out)
            shutil.copyfile(kpointsFile_in, os.path.join(outFolder, 'KPOINTS'))
            shutil.copyfile(potcarFile_in, os.path.join(outFolder, 'POTCAR'))
            shutil.copyfile(poscarFile_in1, os.path.join(outFolder,
                                                        'CONTCAR_'+str(indexList[0])))
            shutil.copyfile(poscarFile_in2, os.path.join(outFolder,
                                                        'CONTCAR_'+str(indexList[1])))
            with open(incarFile_out,'a') as myfile:
                myfile.write("IMAGES = %d" % (inputData["INCAR"]["IMAGES"]))
                os.chdir(outFolder)
            output, input = popen2('bash')
            jobstring = """perl %s CONTCAR_%d CONTCAR_%d %d """ % (inputData["Misc"]["nebscript"], indexList[0],
                                                               indexList[1],inputData["INCAR"]["IMAGES"])
            print outFolder, jobstring
            input.write(jobstring)
            input.close()
            print output.read()
    
   
    return outDirList

def createNebEndptsInputFiles(inputData,dirList):
    for dirVal in dirList:
        incarFile = os.path.join(inputData["OtherInputPath"],
                                 "incarFiles/IBRION_-1_ISMEAR0")
        #kpointsFile = os.path.join(dirVal,inputData["vaspFiles"]["kpointsFile"])
        kpointsFile = os.path.join(inputData["OtherInputPath"],
                                   "kpointsFiles/Gamma_Auto_1x1x1")
        potcarFile = os.path.join(dirVal,inputData["vaspFiles"]["potcarFile"])
        endPt1 = '00'
        endPt2 = '0'+str(inputData["INCAR"]["IMAGES"]+1)
        dirEndPt1 = os.path.join(dirVal, endPt1)
        dirEndPt2 = os.path.join(dirVal, endPt2)
        shutil.copyfile(incarFile, os.path.join(dirEndPt1, 'INCAR'))
        shutil.copyfile(potcarFile, os.path.join(dirEndPt1, 'POTCAR'))
        shutil.copyfile(kpointsFile, os.path.join(dirEndPt1, 'KPOINTS'))
        shutil.copyfile(incarFile, os.path.join(dirEndPt2, 'INCAR'))
        shutil.copyfile(potcarFile, os.path.join(dirEndPt2, 'POTCAR'))
        shutil.copyfile(kpointsFile, os.path.join(dirEndPt2, 'KPOINTS'))
        
        
def getSubDirs(path, level, checkVal):
    dirNameList = []
    for (dirPath, dirNames, fileNames) in walklevel(path, level):
        for dirVal in dirNames:
            if checkdir(dirVal, checkVal):
                dirNameList.append(dirVal)
    return dirNameList
                
if __name__ == '__main__':
    folderPath = '/global/homes/r/rbala/CodesScripts/myCodes/bitbucketRepos/codesscripts/hpcScripts'
    jsonFile = 'inputNebCalc.json'
    with open(os.path.join(folderPath,jsonFile)) as inFile:
        print inFile
        inputData = json.load(inFile)
    if os.path.isdir(inputData["outputPath"]):
        print "OutputPath", inputData["outputPath"], "EXISTS"
    else:
        mkdir_p(inputData["outputPath"]) 

    '''
    ######### PARTA- CREATE STRUCTURE & SUBMIT static calc for endpoints #########
    dirNameList = getSubDirs(inputData["inputPath"],inputData["dirLevel"],
                             inputData["subDirCheckVal"])
    print dirNameList
    
    outDirList = createNebInputFiles(inputData, dirNameList)
    createNebEndptsInputFiles(inputData,outDirList)
    jobId = submitHpcJobsEndpts(inputData, outDirList)
    dirListFile = os.path.join(inputData["outputPath"],'dirList.txt')
    with open(dirListFile, 'w') as dirFile:
        for item in outDirList:
            dirFile.write("%s\n" % item)
    print "Neb Calculation Complete"
    '''

    ########### PART - B/C/D File Readin ###########
    if inputData["calcOptions"]["sdFlag"]:
        dirListFileName = 'dirList_sd.txt'
    else:
        dirListFileName = 'dirList.txt'

    dirListFile = os.path.join(inputData["outputPath"],dirListFileName)
    dirList = [line.rstrip('\n') for line in open(dirListFile, 'r')]

    ######### PARTB - Submit NEB job #########

    '''
    jobId = submitHpcJobsNebCalc(inputData,dirList)
    print 'NebCalc Job Submitted'

    '''

    ######### PARTC - Submit Single Point job #########
    '''
    jobId = submitHpcJobsNebSP(inputData,dirList)
    '''
    ######### PARTC - Calculate the transition energy & plot #########
    
    parseTransitionEnergy(inputData,dirList)
    print 'Transition Energy computed & plotted'
    transitionEnergyList = accumulateTransitionEnergy(inputData,dirList)
    print pprint(transitionEnergyList) #TESTING

    if inputData["calcOptions"]["sdFlag"]:
        transitionListFileName = 'transitionEnergy_sd.txt'
    else:
        transitionListFileName = 'transitionEnergy.txt'

    transitionListFile = os.path.join(inputData["outputPath"],transitionListFileName)
    
    with open(transitionListFile,'w') as outFile:
            writer = csv.writer(outFile)
            writer.writerow(['Dopant', 'Oxygen1-Id', 'Oxygen2-Id', 'Transition-Energy(eV)'])
            writer.writerows(transitionEnergyList)
    
    print 'Transition Energy Computed'
    
